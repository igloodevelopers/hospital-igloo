﻿Shader "Unlit/Outline"
{
	Properties
	{
		_OutlineColor("Outline Color", Color) = (1, 0, 0, 0)
		_ScaleFactor("Scale Factor", Float) = 1.1
	}
		SubShader
	{
		Tags
		{
			"RenderPipeline" = "HDRenderPipeline"
			"RenderType" = "HDUnlitShader"
			"Queue" = "Transparent+0"
		}

		Pass
		{
		// based on UnlitPass.template
		Name "ShadowCaster"
		Tags { "LightMode" = "ShadowCaster" }

		//-------------------------------------------------------------------------------------
		// Render Modes (Blend, Cull, ZTest, Stencil, etc)
		//-------------------------------------------------------------------------------------



		ZWrite On

		Cull Front
		ColorMask 0

		//-------------------------------------------------------------------------------------
		// End Render Modes
		//-------------------------------------------------------------------------------------

		HLSLPROGRAM

		#pragma target 4.5
		#pragma only_renderers d3d11 ps4 xboxone vulkan metal switch
		//#pragma enable_d3d11_debug_symbols

		//enable GPU instancing support
		#pragma multi_compile_instancing

		//-------------------------------------------------------------------------------------
		// Variant Definitions (active field translations to HDRP defines)
		//-------------------------------------------------------------------------------------
		#define _SURFACE_TYPE_TRANSPARENT 1
		#define _BLENDMODE_ALPHA 1
		// #define _BLENDMODE_ADD 1
		// #define _BLENDMODE_PRE_MULTIPLY 1
		// #define _ADD_PRECOMPUTED_VELOCITY

		//-------------------------------------------------------------------------------------
		// End Variant Definitions
		//-------------------------------------------------------------------------------------

		#pragma vertex Vert
		#pragma fragment Frag

		#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
		#include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderVariables.hlsl"
		#include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/FragInputs.hlsl"
		#include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/ShaderPass.cs.hlsl"

		//-------------------------------------------------------------------------------------
		// Defines
		//-------------------------------------------------------------------------------------
				#define SHADERPASS SHADERPASS_SHADOWS
			// ACTIVE FIELDS:
			//   SurfaceType.Transparent
			//   BlendMode.Alpha
			//   VertexDescriptionInputs.ObjectSpaceNormal
			//   VertexDescriptionInputs.ObjectSpaceTangent
			//   VertexDescriptionInputs.ObjectSpacePosition
			//   SurfaceDescription.Alpha
			//   SurfaceDescription.AlphaClipThreshold
			//   features.modifyMesh
			//   VertexDescription.VertexPosition
			//   VertexDescription.VertexNormal
			//   VertexDescription.VertexTangent
			//   AttributesMesh.normalOS
			//   AttributesMesh.tangentOS
			//   AttributesMesh.positionOS
			// Shared Graph Keywords

		// this translates the new dependency tracker into the old preprocessor definitions for the existing HDRP shader code
		#define ATTRIBUTES_NEED_NORMAL
		#define ATTRIBUTES_NEED_TANGENT
		// #define ATTRIBUTES_NEED_TEXCOORD0
		// #define ATTRIBUTES_NEED_TEXCOORD1
		// #define ATTRIBUTES_NEED_TEXCOORD2
		// #define ATTRIBUTES_NEED_TEXCOORD3
		// #define ATTRIBUTES_NEED_COLOR
		// #define VARYINGS_NEED_POSITION_WS
		// #define VARYINGS_NEED_TANGENT_TO_WORLD
		// #define VARYINGS_NEED_TEXCOORD0
		// #define VARYINGS_NEED_TEXCOORD1
		// #define VARYINGS_NEED_TEXCOORD2
		// #define VARYINGS_NEED_TEXCOORD3
		// #define VARYINGS_NEED_COLOR
		// #define VARYINGS_NEED_CULLFACE
		#define HAVE_MESH_MODIFICATION

		//-------------------------------------------------------------------------------------
		// End Defines
		//-------------------------------------------------------------------------------------


		#include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/Material.hlsl"
		#include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/Unlit/Unlit.hlsl"

		#include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/BuiltinUtilities.hlsl"
		#include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/MaterialUtilities.hlsl"
		#include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderGraphFunctions.hlsl"

		// Used by SceneSelectionPass
		int _ObjectId;
		int _PassValue;

		//-------------------------------------------------------------------------------------
		// Interpolator Packing And Struct Declarations
		//-------------------------------------------------------------------------------------
		// Generated Type: AttributesMesh
		struct AttributesMesh
		{
			float3 positionOS : POSITION;
			float3 normalOS : NORMAL; // optional
			float4 tangentOS : TANGENT; // optional
			#if UNITY_ANY_INSTANCING_ENABLED
			uint instanceID : INSTANCEID_SEMANTIC;
			#endif // UNITY_ANY_INSTANCING_ENABLED
		};
		// Generated Type: VaryingsMeshToPS
		struct VaryingsMeshToPS
		{
			float4 positionCS : SV_POSITION;
			#if UNITY_ANY_INSTANCING_ENABLED
			uint instanceID : CUSTOM_INSTANCE_ID;
			#endif // UNITY_ANY_INSTANCING_ENABLED
			#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
			FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
			#endif // defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		};

		// Generated Type: PackedVaryingsMeshToPS
		struct PackedVaryingsMeshToPS
		{
			float4 positionCS : SV_POSITION; // unpacked
			#if UNITY_ANY_INSTANCING_ENABLED
			uint instanceID : CUSTOM_INSTANCE_ID; // unpacked
			#endif // conditional
			#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
			FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC; // unpacked
			#endif // conditional
		};

		// Packed Type: VaryingsMeshToPS
		PackedVaryingsMeshToPS PackVaryingsMeshToPS(VaryingsMeshToPS input)
		{
			PackedVaryingsMeshToPS output = (PackedVaryingsMeshToPS)0;
			output.positionCS = input.positionCS;
			#if UNITY_ANY_INSTANCING_ENABLED
			output.instanceID = input.instanceID;
			#endif // conditional
			#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
			output.cullFace = input.cullFace;
			#endif // conditional
			return output;
		}

		// Unpacked Type: VaryingsMeshToPS
		VaryingsMeshToPS UnpackVaryingsMeshToPS(PackedVaryingsMeshToPS input)
		{
			VaryingsMeshToPS output = (VaryingsMeshToPS)0;
			output.positionCS = input.positionCS;
			#if UNITY_ANY_INSTANCING_ENABLED
			output.instanceID = input.instanceID;
			#endif // conditional
			#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
			output.cullFace = input.cullFace;
			#endif // conditional
			return output;
		}
		// Generated Type: VaryingsMeshToDS
		struct VaryingsMeshToDS
		{
			float3 positionRWS;
			float3 normalWS;
			#if UNITY_ANY_INSTANCING_ENABLED
			uint instanceID : CUSTOM_INSTANCE_ID;
			#endif // UNITY_ANY_INSTANCING_ENABLED
		};

		// Generated Type: PackedVaryingsMeshToDS
		struct PackedVaryingsMeshToDS
		{
			#if UNITY_ANY_INSTANCING_ENABLED
			uint instanceID : CUSTOM_INSTANCE_ID; // unpacked
			#endif // conditional
			float3 interp00 : TEXCOORD0; // auto-packed
			float3 interp01 : TEXCOORD1; // auto-packed
		};

		// Packed Type: VaryingsMeshToDS
		PackedVaryingsMeshToDS PackVaryingsMeshToDS(VaryingsMeshToDS input)
		{
			PackedVaryingsMeshToDS output = (PackedVaryingsMeshToDS)0;
			output.interp00.xyz = input.positionRWS;
			output.interp01.xyz = input.normalWS;
			#if UNITY_ANY_INSTANCING_ENABLED
			output.instanceID = input.instanceID;
			#endif // conditional
			return output;
		}

		// Unpacked Type: VaryingsMeshToDS
		VaryingsMeshToDS UnpackVaryingsMeshToDS(PackedVaryingsMeshToDS input)
		{
			VaryingsMeshToDS output = (VaryingsMeshToDS)0;
			output.positionRWS = input.interp00.xyz;
			output.normalWS = input.interp01.xyz;
			#if UNITY_ANY_INSTANCING_ENABLED
			output.instanceID = input.instanceID;
			#endif // conditional
			return output;
		}
		//-------------------------------------------------------------------------------------
		// End Interpolator Packing And Struct Declarations
		//-------------------------------------------------------------------------------------

		//-------------------------------------------------------------------------------------
		// Graph generated code
		//-------------------------------------------------------------------------------------
				// Shared Graph Properties (uniform inputs)
				CBUFFER_START(UnityPerMaterial)
				float4 _OutlineColor;
				float _ScaleFactor;
				CBUFFER_END

					// Vertex Graph Inputs
						struct VertexDescriptionInputs
						{
							float3 ObjectSpaceNormal; // optional
							float3 ObjectSpaceTangent; // optional
							float3 ObjectSpacePosition; // optional
						};
				// Vertex Graph Outputs
					struct VertexDescription
					{
						float3 VertexPosition;
						float3 VertexNormal;
						float3 VertexTangent;
					};

					// Pixel Graph Inputs
						struct SurfaceDescriptionInputs
						{
						};
						// Pixel Graph Outputs
							struct SurfaceDescription
							{
								float Alpha;
								float AlphaClipThreshold;
							};

							// Shared Graph Node Functions

								void Unity_Multiply_float(float3 A, float3 B, out float3 Out)
								{
									Out = A * B;
								}

								// Vertex Graph Evaluation
									VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
									{
										VertexDescription description = (VertexDescription)0;
										float _Property_FE6E722B_Out_0 = _ScaleFactor;
										float3 _Multiply_10848E78_Out_2;
										Unity_Multiply_float((_Property_FE6E722B_Out_0.xxx), IN.ObjectSpacePosition, _Multiply_10848E78_Out_2);
										description.VertexPosition = _Multiply_10848E78_Out_2;
										description.VertexNormal = IN.ObjectSpaceNormal;
										description.VertexTangent = IN.ObjectSpaceTangent;
										return description;
									}

									// Pixel Graph Evaluation
										SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
										{
											SurfaceDescription surface = (SurfaceDescription)0;
											surface.Alpha = 1;
											surface.AlphaClipThreshold = 0;
											return surface;
										}

										//-------------------------------------------------------------------------------------
										// End graph generated code
										//-------------------------------------------------------------------------------------

									//-------------------------------------------------------------------------------------
										// TEMPLATE INCLUDE : VertexAnimation.template.hlsl
										//-------------------------------------------------------------------------------------


										VertexDescriptionInputs AttributesMeshToVertexDescriptionInputs(AttributesMesh input)
										{
											VertexDescriptionInputs output;
											ZERO_INITIALIZE(VertexDescriptionInputs, output);

											output.ObjectSpaceNormal = input.normalOS;
											// output.WorldSpaceNormal =            TransformObjectToWorldNormal(input.normalOS);
											// output.ViewSpaceNormal =             TransformWorldToViewDir(output.WorldSpaceNormal);
											// output.TangentSpaceNormal =          float3(0.0f, 0.0f, 1.0f);
											output.ObjectSpaceTangent = input.tangentOS;
											// output.WorldSpaceTangent =           TransformObjectToWorldDir(input.tangentOS.xyz);
											// output.ViewSpaceTangent =            TransformWorldToViewDir(output.WorldSpaceTangent);
											// output.TangentSpaceTangent =         float3(1.0f, 0.0f, 0.0f);
											// output.ObjectSpaceBiTangent =        normalize(cross(input.normalOS, input.tangentOS) * (input.tangentOS.w > 0.0f ? 1.0f : -1.0f) * GetOddNegativeScale());
											// output.WorldSpaceBiTangent =         TransformObjectToWorldDir(output.ObjectSpaceBiTangent);
											// output.ViewSpaceBiTangent =          TransformWorldToViewDir(output.WorldSpaceBiTangent);
											// output.TangentSpaceBiTangent =       float3(0.0f, 1.0f, 0.0f);
											output.ObjectSpacePosition = input.positionOS;
											// output.WorldSpacePosition =          TransformObjectToWorld(input.positionOS);
											// output.ViewSpacePosition =           TransformWorldToView(output.WorldSpacePosition);
											// output.TangentSpacePosition =        float3(0.0f, 0.0f, 0.0f);
											// output.AbsoluteWorldSpacePosition =  GetAbsolutePositionWS(TransformObjectToWorld(input.positionOS));
											// output.WorldSpaceViewDirection =     GetWorldSpaceNormalizeViewDir(output.WorldSpacePosition);
											// output.ObjectSpaceViewDirection =    TransformWorldToObjectDir(output.WorldSpaceViewDirection);
											// output.ViewSpaceViewDirection =      TransformWorldToViewDir(output.WorldSpaceViewDirection);
											// float3x3 tangentSpaceTransform =     float3x3(output.WorldSpaceTangent,output.WorldSpaceBiTangent,output.WorldSpaceNormal);
											// output.TangentSpaceViewDirection =   mul(tangentSpaceTransform, output.WorldSpaceViewDirection);
											// output.ScreenPosition =              ComputeScreenPos(TransformWorldToHClip(output.WorldSpacePosition), _ProjectionParams.x);
											// output.uv0 =                         input.uv0;
											// output.uv1 =                         input.uv1;
											// output.uv2 =                         input.uv2;
											// output.uv3 =                         input.uv3;
											// output.VertexColor =                 input.color;
											// output.BoneWeights =                 input.weights;
											// output.BoneIndices =                 input.indices;

											return output;
										}

										AttributesMesh ApplyMeshModification(AttributesMesh input, float3 timeParameters)
										{
											// build graph inputs
											VertexDescriptionInputs vertexDescriptionInputs = AttributesMeshToVertexDescriptionInputs(input);
											// Override time paramters with used one (This is required to correctly handle motion vector for vertex animation based on time)
											// vertexDescriptionInputs.TimeParameters = timeParameters;

											// evaluate vertex graph
											VertexDescription vertexDescription = VertexDescriptionFunction(vertexDescriptionInputs);

											// copy graph output to the results
											input.positionOS = vertexDescription.VertexPosition;
											input.normalOS = vertexDescription.VertexNormal;
											input.tangentOS.xyz = vertexDescription.VertexTangent;

											return input;
										}

										//-------------------------------------------------------------------------------------
										// END TEMPLATE INCLUDE : VertexAnimation.template.hlsl
										//-------------------------------------------------------------------------------------


									//-------------------------------------------------------------------------------------
										// TEMPLATE INCLUDE : SharedCode.template.hlsl
										//-------------------------------------------------------------------------------------

										#if !defined(SHADER_STAGE_RAY_TRACING)
											FragInputs BuildFragInputs(VaryingsMeshToPS input)
											{
												FragInputs output;
												ZERO_INITIALIZE(FragInputs, output);

												// Init to some default value to make the computer quiet (else it output 'divide by zero' warning even if value is not used).
												// TODO: this is a really poor workaround, but the variable is used in a bunch of places
												// to compute normals which are then passed on elsewhere to compute other values...
												output.tangentToWorld = k_identity3x3;
												output.positionSS = input.positionCS;       // input.positionCS is SV_Position

												// output.positionRWS = input.positionRWS;
												// output.tangentToWorld = BuildTangentToWorld(input.tangentWS, input.normalWS);
												// output.texCoord0 = input.texCoord0;
												// output.texCoord1 = input.texCoord1;
												// output.texCoord2 = input.texCoord2;
												// output.texCoord3 = input.texCoord3;
												// output.color = input.color;
												#if _DOUBLESIDED_ON && SHADER_STAGE_FRAGMENT
												output.isFrontFace = IS_FRONT_VFACE(input.cullFace, true, false);
												#elif SHADER_STAGE_FRAGMENT
												// output.isFrontFace = IS_FRONT_VFACE(input.cullFace, true, false);
												#endif // SHADER_STAGE_FRAGMENT

												return output;
											}
										#endif
											SurfaceDescriptionInputs FragInputsToSurfaceDescriptionInputs(FragInputs input, float3 viewWS)
											{
												SurfaceDescriptionInputs output;
												ZERO_INITIALIZE(SurfaceDescriptionInputs, output);

												// output.WorldSpaceNormal =            input.tangentToWorld[2].xyz;	// normal was already normalized in BuildTangentToWorld()
												// output.ObjectSpaceNormal =           normalize(mul(output.WorldSpaceNormal, (float3x3) UNITY_MATRIX_M));           // transposed multiplication by inverse matrix to handle normal scale
												// output.ViewSpaceNormal =             mul(output.WorldSpaceNormal, (float3x3) UNITY_MATRIX_I_V);         // transposed multiplication by inverse matrix to handle normal scale
												// output.TangentSpaceNormal =          float3(0.0f, 0.0f, 1.0f);
												// output.WorldSpaceTangent =           input.tangentToWorld[0].xyz;
												// output.ObjectSpaceTangent =          TransformWorldToObjectDir(output.WorldSpaceTangent);
												// output.ViewSpaceTangent =            TransformWorldToViewDir(output.WorldSpaceTangent);
												// output.TangentSpaceTangent =         float3(1.0f, 0.0f, 0.0f);
												// output.WorldSpaceBiTangent =         input.tangentToWorld[1].xyz;
												// output.ObjectSpaceBiTangent =        TransformWorldToObjectDir(output.WorldSpaceBiTangent);
												// output.ViewSpaceBiTangent =          TransformWorldToViewDir(output.WorldSpaceBiTangent);
												// output.TangentSpaceBiTangent =       float3(0.0f, 1.0f, 0.0f);
												// output.WorldSpaceViewDirection =     normalize(viewWS);
												// output.ObjectSpaceViewDirection =    TransformWorldToObjectDir(output.WorldSpaceViewDirection);
												// output.ViewSpaceViewDirection =      TransformWorldToViewDir(output.WorldSpaceViewDirection);
												// float3x3 tangentSpaceTransform =     float3x3(output.WorldSpaceTangent,output.WorldSpaceBiTangent,output.WorldSpaceNormal);
												// output.TangentSpaceViewDirection =   mul(tangentSpaceTransform, output.WorldSpaceViewDirection);
												// output.WorldSpacePosition =          input.positionRWS;
												// output.ObjectSpacePosition =         TransformWorldToObject(input.positionRWS);
												// output.ViewSpacePosition =           TransformWorldToView(input.positionRWS);
												// output.TangentSpacePosition =        float3(0.0f, 0.0f, 0.0f);
												// output.AbsoluteWorldSpacePosition =  GetAbsolutePositionWS(input.positionRWS);
												// output.ScreenPosition =              ComputeScreenPos(TransformWorldToHClip(input.positionRWS), _ProjectionParams.x);
												// output.uv0 =                         input.texCoord0;
												// output.uv1 =                         input.texCoord1;
												// output.uv2 =                         input.texCoord2;
												// output.uv3 =                         input.texCoord3;
												// output.VertexColor =                 input.color;
												// output.FaceSign =                    input.isFrontFace;
												// output.TimeParameters =              _TimeParameters.xyz; // This is mainly for LW as HD overwrite this value

												return output;
											}

										#if !defined(SHADER_STAGE_RAY_TRACING)

											// existing HDRP code uses the combined function to go directly from packed to frag inputs
											FragInputs UnpackVaryingsMeshToFragInputs(PackedVaryingsMeshToPS input)
											{
												UNITY_SETUP_INSTANCE_ID(input);
												VaryingsMeshToPS unpacked = UnpackVaryingsMeshToPS(input);
												return BuildFragInputs(unpacked);
											}
										#endif

											//-------------------------------------------------------------------------------------
											// END TEMPLATE INCLUDE : SharedCode.template.hlsl
											//-------------------------------------------------------------------------------------



											void BuildSurfaceData(FragInputs fragInputs, inout SurfaceDescription surfaceDescription, float3 V, PositionInputs posInput, out SurfaceData surfaceData)
											{
												// setup defaults -- these are used if the graph doesn't output a value
												ZERO_INITIALIZE(SurfaceData, surfaceData);

												// copy across graph values, if defined
												// surfaceData.color = surfaceDescription.Color;

										#if defined(DEBUG_DISPLAY)
												if (_DebugMipMapMode != DEBUGMIPMAPMODE_NONE)
												{
													// TODO
												}
										#endif
											}

											void GetSurfaceAndBuiltinData(FragInputs fragInputs, float3 V, inout PositionInputs posInput, out SurfaceData surfaceData, out BuiltinData builtinData)
											{
												SurfaceDescriptionInputs surfaceDescriptionInputs = FragInputsToSurfaceDescriptionInputs(fragInputs, V);
												SurfaceDescription surfaceDescription = SurfaceDescriptionFunction(surfaceDescriptionInputs);

												// Perform alpha test very early to save performance (a killed pixel will not sample textures)
												// TODO: split graph evaluation to grab just alpha dependencies first? tricky..
												// DoAlphaTest(surfaceDescription.Alpha, surfaceDescription.AlphaClipThreshold);

												BuildSurfaceData(fragInputs, surfaceDescription, V, posInput, surfaceData);

												// Builtin Data
												ZERO_INITIALIZE(BuiltinData, builtinData); // No call to InitBuiltinData as we don't have any lighting
												builtinData.opacity = surfaceDescription.Alpha;
											}

											//-------------------------------------------------------------------------------------
											// Pass Includes
											//-------------------------------------------------------------------------------------
												#include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/ShaderPassDepthOnly.hlsl"
											//-------------------------------------------------------------------------------------
											// End Pass Includes
											//-------------------------------------------------------------------------------------

											ENDHLSL
										}

										Pass
										{
												// based on UnlitPass.template
												Name "META"
												Tags { "LightMode" = "META" }

												//-------------------------------------------------------------------------------------
												// Render Modes (Blend, Cull, ZTest, Stencil, etc)
												//-------------------------------------------------------------------------------------

												Cull Off





												//-------------------------------------------------------------------------------------
												// End Render Modes
												//-------------------------------------------------------------------------------------

												HLSLPROGRAM

												#pragma target 4.5
												#pragma only_renderers d3d11 ps4 xboxone vulkan metal switch
												//#pragma enable_d3d11_debug_symbols

												//enable GPU instancing support
												#pragma multi_compile_instancing

												//-------------------------------------------------------------------------------------
												// Variant Definitions (active field translations to HDRP defines)
												//-------------------------------------------------------------------------------------
												#define _SURFACE_TYPE_TRANSPARENT 1
												#define _BLENDMODE_ALPHA 1
												// #define _BLENDMODE_ADD 1
												// #define _BLENDMODE_PRE_MULTIPLY 1
												// #define _ADD_PRECOMPUTED_VELOCITY

												//-------------------------------------------------------------------------------------
												// End Variant Definitions
												//-------------------------------------------------------------------------------------

												#pragma vertex Vert
												#pragma fragment Frag

												#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
												#include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderVariables.hlsl"
												#include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/FragInputs.hlsl"
												#include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/ShaderPass.cs.hlsl"

												//-------------------------------------------------------------------------------------
												// Defines
												//-------------------------------------------------------------------------------------
														#define SHADERPASS SHADERPASS_LIGHT_TRANSPORT
													// ACTIVE FIELDS:
													//   SurfaceType.Transparent
													//   BlendMode.Alpha
													//   VertexDescriptionInputs.ObjectSpaceNormal
													//   VertexDescriptionInputs.ObjectSpaceTangent
													//   SurfaceDescription.Color
													//   SurfaceDescription.Alpha
													//   SurfaceDescription.AlphaClipThreshold
													//   features.modifyMesh
													//   AttributesMesh.normalOS
													//   AttributesMesh.tangentOS
													//   AttributesMesh.uv0
													//   AttributesMesh.uv1
													//   AttributesMesh.color
													//   AttributesMesh.uv2
													// Shared Graph Keywords

												// this translates the new dependency tracker into the old preprocessor definitions for the existing HDRP shader code
												#define ATTRIBUTES_NEED_NORMAL
												#define ATTRIBUTES_NEED_TANGENT
												#define ATTRIBUTES_NEED_TEXCOORD0
												#define ATTRIBUTES_NEED_TEXCOORD1
												#define ATTRIBUTES_NEED_TEXCOORD2
												// #define ATTRIBUTES_NEED_TEXCOORD3
												#define ATTRIBUTES_NEED_COLOR
												// #define VARYINGS_NEED_POSITION_WS
												// #define VARYINGS_NEED_TANGENT_TO_WORLD
												// #define VARYINGS_NEED_TEXCOORD0
												// #define VARYINGS_NEED_TEXCOORD1
												// #define VARYINGS_NEED_TEXCOORD2
												// #define VARYINGS_NEED_TEXCOORD3
												// #define VARYINGS_NEED_COLOR
												// #define VARYINGS_NEED_CULLFACE
												#define HAVE_MESH_MODIFICATION

												//-------------------------------------------------------------------------------------
												// End Defines
												//-------------------------------------------------------------------------------------


												#include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/Material.hlsl"
												#include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/Unlit/Unlit.hlsl"

												#include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/BuiltinUtilities.hlsl"
												#include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/MaterialUtilities.hlsl"
												#include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderGraphFunctions.hlsl"

												// Used by SceneSelectionPass
												int _ObjectId;
												int _PassValue;

												//-------------------------------------------------------------------------------------
												// Interpolator Packing And Struct Declarations
												//-------------------------------------------------------------------------------------
												// Generated Type: AttributesMesh
												struct AttributesMesh
												{
													float3 positionOS : POSITION;
													float3 normalOS : NORMAL; // optional
													float4 tangentOS : TANGENT; // optional
													float4 uv0 : TEXCOORD0; // optional
													float4 uv1 : TEXCOORD1; // optional
													float4 uv2 : TEXCOORD2; // optional
													float4 color : COLOR; // optional
													#if UNITY_ANY_INSTANCING_ENABLED
													uint instanceID : INSTANCEID_SEMANTIC;
													#endif // UNITY_ANY_INSTANCING_ENABLED
												};
												// Generated Type: VaryingsMeshToPS
												struct VaryingsMeshToPS
												{
													float4 positionCS : SV_POSITION;
													#if UNITY_ANY_INSTANCING_ENABLED
													uint instanceID : CUSTOM_INSTANCE_ID;
													#endif // UNITY_ANY_INSTANCING_ENABLED
													#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
													FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
													#endif // defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
												};

												// Generated Type: PackedVaryingsMeshToPS
												struct PackedVaryingsMeshToPS
												{
													float4 positionCS : SV_POSITION; // unpacked
													#if UNITY_ANY_INSTANCING_ENABLED
													uint instanceID : CUSTOM_INSTANCE_ID; // unpacked
													#endif // conditional
													#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
													FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC; // unpacked
													#endif // conditional
												};

												// Packed Type: VaryingsMeshToPS
												PackedVaryingsMeshToPS PackVaryingsMeshToPS(VaryingsMeshToPS input)
												{
													PackedVaryingsMeshToPS output = (PackedVaryingsMeshToPS)0;
													output.positionCS = input.positionCS;
													#if UNITY_ANY_INSTANCING_ENABLED
													output.instanceID = input.instanceID;
													#endif // conditional
													#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
													output.cullFace = input.cullFace;
													#endif // conditional
													return output;
												}

												// Unpacked Type: VaryingsMeshToPS
												VaryingsMeshToPS UnpackVaryingsMeshToPS(PackedVaryingsMeshToPS input)
												{
													VaryingsMeshToPS output = (VaryingsMeshToPS)0;
													output.positionCS = input.positionCS;
													#if UNITY_ANY_INSTANCING_ENABLED
													output.instanceID = input.instanceID;
													#endif // conditional
													#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
													output.cullFace = input.cullFace;
													#endif // conditional
													return output;
												}
												// Generated Type: VaryingsMeshToDS
												struct VaryingsMeshToDS
												{
													float3 positionRWS;
													float3 normalWS;
													#if UNITY_ANY_INSTANCING_ENABLED
													uint instanceID : CUSTOM_INSTANCE_ID;
													#endif // UNITY_ANY_INSTANCING_ENABLED
												};

												// Generated Type: PackedVaryingsMeshToDS
												struct PackedVaryingsMeshToDS
												{
													#if UNITY_ANY_INSTANCING_ENABLED
													uint instanceID : CUSTOM_INSTANCE_ID; // unpacked
													#endif // conditional
													float3 interp00 : TEXCOORD0; // auto-packed
													float3 interp01 : TEXCOORD1; // auto-packed
												};

												// Packed Type: VaryingsMeshToDS
												PackedVaryingsMeshToDS PackVaryingsMeshToDS(VaryingsMeshToDS input)
												{
													PackedVaryingsMeshToDS output = (PackedVaryingsMeshToDS)0;
													output.interp00.xyz = input.positionRWS;
													output.interp01.xyz = input.normalWS;
													#if UNITY_ANY_INSTANCING_ENABLED
													output.instanceID = input.instanceID;
													#endif // conditional
													return output;
												}

												// Unpacked Type: VaryingsMeshToDS
												VaryingsMeshToDS UnpackVaryingsMeshToDS(PackedVaryingsMeshToDS input)
												{
													VaryingsMeshToDS output = (VaryingsMeshToDS)0;
													output.positionRWS = input.interp00.xyz;
													output.normalWS = input.interp01.xyz;
													#if UNITY_ANY_INSTANCING_ENABLED
													output.instanceID = input.instanceID;
													#endif // conditional
													return output;
												}
												//-------------------------------------------------------------------------------------
												// End Interpolator Packing And Struct Declarations
												//-------------------------------------------------------------------------------------

												//-------------------------------------------------------------------------------------
												// Graph generated code
												//-------------------------------------------------------------------------------------
														// Shared Graph Properties (uniform inputs)
														CBUFFER_START(UnityPerMaterial)
														float4 _OutlineColor;
														float _ScaleFactor;
														CBUFFER_END

															// Vertex Graph Inputs
																struct VertexDescriptionInputs
																{
																	float3 ObjectSpaceNormal; // optional
																	float3 ObjectSpaceTangent; // optional
																};
														// Vertex Graph Outputs
															struct VertexDescription
															{
															};

															// Pixel Graph Inputs
																struct SurfaceDescriptionInputs
																{
																};
																// Pixel Graph Outputs
																	struct SurfaceDescription
																	{
																		float3 Color;
																		float Alpha;
																		float AlphaClipThreshold;
																	};

																	// Shared Graph Node Functions
																	// Vertex Graph Evaluation
																		VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
																		{
																			VertexDescription description = (VertexDescription)0;
																			return description;
																		}

																		// Pixel Graph Evaluation
																			SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
																			{
																				SurfaceDescription surface = (SurfaceDescription)0;
																				float4 _Property_C8A34B75_Out_0 = _OutlineColor;
																				surface.Color = (_Property_C8A34B75_Out_0.xyz);
																				surface.Alpha = 1;
																				surface.AlphaClipThreshold = 0;
																				return surface;
																			}

																			//-------------------------------------------------------------------------------------
																			// End graph generated code
																			//-------------------------------------------------------------------------------------

																		//-------------------------------------------------------------------------------------
																			// TEMPLATE INCLUDE : VertexAnimation.template.hlsl
																			//-------------------------------------------------------------------------------------


																			VertexDescriptionInputs AttributesMeshToVertexDescriptionInputs(AttributesMesh input)
																			{
																				VertexDescriptionInputs output;
																				ZERO_INITIALIZE(VertexDescriptionInputs, output);

																				output.ObjectSpaceNormal = input.normalOS;
																				// output.WorldSpaceNormal =            TransformObjectToWorldNormal(input.normalOS);
																				// output.ViewSpaceNormal =             TransformWorldToViewDir(output.WorldSpaceNormal);
																				// output.TangentSpaceNormal =          float3(0.0f, 0.0f, 1.0f);
																				output.ObjectSpaceTangent = input.tangentOS;
																				// output.WorldSpaceTangent =           TransformObjectToWorldDir(input.tangentOS.xyz);
																				// output.ViewSpaceTangent =            TransformWorldToViewDir(output.WorldSpaceTangent);
																				// output.TangentSpaceTangent =         float3(1.0f, 0.0f, 0.0f);
																				// output.ObjectSpaceBiTangent =        normalize(cross(input.normalOS, input.tangentOS) * (input.tangentOS.w > 0.0f ? 1.0f : -1.0f) * GetOddNegativeScale());
																				// output.WorldSpaceBiTangent =         TransformObjectToWorldDir(output.ObjectSpaceBiTangent);
																				// output.ViewSpaceBiTangent =          TransformWorldToViewDir(output.WorldSpaceBiTangent);
																				// output.TangentSpaceBiTangent =       float3(0.0f, 1.0f, 0.0f);
																				// output.ObjectSpacePosition =         input.positionOS;
																				// output.WorldSpacePosition =          TransformObjectToWorld(input.positionOS);
																				// output.ViewSpacePosition =           TransformWorldToView(output.WorldSpacePosition);
																				// output.TangentSpacePosition =        float3(0.0f, 0.0f, 0.0f);
																				// output.AbsoluteWorldSpacePosition =  GetAbsolutePositionWS(TransformObjectToWorld(input.positionOS));
																				// output.WorldSpaceViewDirection =     GetWorldSpaceNormalizeViewDir(output.WorldSpacePosition);
																				// output.ObjectSpaceViewDirection =    TransformWorldToObjectDir(output.WorldSpaceViewDirection);
																				// output.ViewSpaceViewDirection =      TransformWorldToViewDir(output.WorldSpaceViewDirection);
																				// float3x3 tangentSpaceTransform =     float3x3(output.WorldSpaceTangent,output.WorldSpaceBiTangent,output.WorldSpaceNormal);
																				// output.TangentSpaceViewDirection =   mul(tangentSpaceTransform, output.WorldSpaceViewDirection);
																				// output.ScreenPosition =              ComputeScreenPos(TransformWorldToHClip(output.WorldSpacePosition), _ProjectionParams.x);
																				// output.uv0 =                         input.uv0;
																				// output.uv1 =                         input.uv1;
																				// output.uv2 =                         input.uv2;
																				// output.uv3 =                         input.uv3;
																				// output.VertexColor =                 input.color;
																				// output.BoneWeights =                 input.weights;
																				// output.BoneIndices =                 input.indices;

																				return output;
																			}

																			AttributesMesh ApplyMeshModification(AttributesMesh input, float3 timeParameters)
																			{
																				// build graph inputs
																				VertexDescriptionInputs vertexDescriptionInputs = AttributesMeshToVertexDescriptionInputs(input);
																				// Override time paramters with used one (This is required to correctly handle motion vector for vertex animation based on time)
																				// vertexDescriptionInputs.TimeParameters = timeParameters;

																				// evaluate vertex graph
																				VertexDescription vertexDescription = VertexDescriptionFunction(vertexDescriptionInputs);

																				// copy graph output to the results
																				// input.positionOS = vertexDescription.VertexPosition;
																				// input.normalOS = vertexDescription.VertexNormal;
																				// input.tangentOS.xyz = vertexDescription.VertexTangent;

																				return input;
																			}

																			//-------------------------------------------------------------------------------------
																			// END TEMPLATE INCLUDE : VertexAnimation.template.hlsl
																			//-------------------------------------------------------------------------------------


																		//-------------------------------------------------------------------------------------
																			// TEMPLATE INCLUDE : SharedCode.template.hlsl
																			//-------------------------------------------------------------------------------------

																			#if !defined(SHADER_STAGE_RAY_TRACING)
																				FragInputs BuildFragInputs(VaryingsMeshToPS input)
																				{
																					FragInputs output;
																					ZERO_INITIALIZE(FragInputs, output);

																					// Init to some default value to make the computer quiet (else it output 'divide by zero' warning even if value is not used).
																					// TODO: this is a really poor workaround, but the variable is used in a bunch of places
																					// to compute normals which are then passed on elsewhere to compute other values...
																					output.tangentToWorld = k_identity3x3;
																					output.positionSS = input.positionCS;       // input.positionCS is SV_Position

																					// output.positionRWS = input.positionRWS;
																					// output.tangentToWorld = BuildTangentToWorld(input.tangentWS, input.normalWS);
																					// output.texCoord0 = input.texCoord0;
																					// output.texCoord1 = input.texCoord1;
																					// output.texCoord2 = input.texCoord2;
																					// output.texCoord3 = input.texCoord3;
																					// output.color = input.color;
																					#if _DOUBLESIDED_ON && SHADER_STAGE_FRAGMENT
																					output.isFrontFace = IS_FRONT_VFACE(input.cullFace, true, false);
																					#elif SHADER_STAGE_FRAGMENT
																					// output.isFrontFace = IS_FRONT_VFACE(input.cullFace, true, false);
																					#endif // SHADER_STAGE_FRAGMENT

																					return output;
																				}
																			#endif
																				SurfaceDescriptionInputs FragInputsToSurfaceDescriptionInputs(FragInputs input, float3 viewWS)
																				{
																					SurfaceDescriptionInputs output;
																					ZERO_INITIALIZE(SurfaceDescriptionInputs, output);

																					// output.WorldSpaceNormal =            input.tangentToWorld[2].xyz;	// normal was already normalized in BuildTangentToWorld()
																					// output.ObjectSpaceNormal =           normalize(mul(output.WorldSpaceNormal, (float3x3) UNITY_MATRIX_M));           // transposed multiplication by inverse matrix to handle normal scale
																					// output.ViewSpaceNormal =             mul(output.WorldSpaceNormal, (float3x3) UNITY_MATRIX_I_V);         // transposed multiplication by inverse matrix to handle normal scale
																					// output.TangentSpaceNormal =          float3(0.0f, 0.0f, 1.0f);
																					// output.WorldSpaceTangent =           input.tangentToWorld[0].xyz;
																					// output.ObjectSpaceTangent =          TransformWorldToObjectDir(output.WorldSpaceTangent);
																					// output.ViewSpaceTangent =            TransformWorldToViewDir(output.WorldSpaceTangent);
																					// output.TangentSpaceTangent =         float3(1.0f, 0.0f, 0.0f);
																					// output.WorldSpaceBiTangent =         input.tangentToWorld[1].xyz;
																					// output.ObjectSpaceBiTangent =        TransformWorldToObjectDir(output.WorldSpaceBiTangent);
																					// output.ViewSpaceBiTangent =          TransformWorldToViewDir(output.WorldSpaceBiTangent);
																					// output.TangentSpaceBiTangent =       float3(0.0f, 1.0f, 0.0f);
																					// output.WorldSpaceViewDirection =     normalize(viewWS);
																					// output.ObjectSpaceViewDirection =    TransformWorldToObjectDir(output.WorldSpaceViewDirection);
																					// output.ViewSpaceViewDirection =      TransformWorldToViewDir(output.WorldSpaceViewDirection);
																					// float3x3 tangentSpaceTransform =     float3x3(output.WorldSpaceTangent,output.WorldSpaceBiTangent,output.WorldSpaceNormal);
																					// output.TangentSpaceViewDirection =   mul(tangentSpaceTransform, output.WorldSpaceViewDirection);
																					// output.WorldSpacePosition =          input.positionRWS;
																					// output.ObjectSpacePosition =         TransformWorldToObject(input.positionRWS);
																					// output.ViewSpacePosition =           TransformWorldToView(input.positionRWS);
																					// output.TangentSpacePosition =        float3(0.0f, 0.0f, 0.0f);
																					// output.AbsoluteWorldSpacePosition =  GetAbsolutePositionWS(input.positionRWS);
																					// output.ScreenPosition =              ComputeScreenPos(TransformWorldToHClip(input.positionRWS), _ProjectionParams.x);
																					// output.uv0 =                         input.texCoord0;
																					// output.uv1 =                         input.texCoord1;
																					// output.uv2 =                         input.texCoord2;
																					// output.uv3 =                         input.texCoord3;
																					// output.VertexColor =                 input.color;
																					// output.FaceSign =                    input.isFrontFace;
																					// output.TimeParameters =              _TimeParameters.xyz; // This is mainly for LW as HD overwrite this value

																					return output;
																				}

																			#if !defined(SHADER_STAGE_RAY_TRACING)

																				// existing HDRP code uses the combined function to go directly from packed to frag inputs
																				FragInputs UnpackVaryingsMeshToFragInputs(PackedVaryingsMeshToPS input)
																				{
																					UNITY_SETUP_INSTANCE_ID(input);
																					VaryingsMeshToPS unpacked = UnpackVaryingsMeshToPS(input);
																					return BuildFragInputs(unpacked);
																				}
																			#endif

																				//-------------------------------------------------------------------------------------
																				// END TEMPLATE INCLUDE : SharedCode.template.hlsl
																				//-------------------------------------------------------------------------------------



																				void BuildSurfaceData(FragInputs fragInputs, inout SurfaceDescription surfaceDescription, float3 V, PositionInputs posInput, out SurfaceData surfaceData)
																				{
																					// setup defaults -- these are used if the graph doesn't output a value
																					ZERO_INITIALIZE(SurfaceData, surfaceData);

																					// copy across graph values, if defined
																					surfaceData.color = surfaceDescription.Color;

																			#if defined(DEBUG_DISPLAY)
																					if (_DebugMipMapMode != DEBUGMIPMAPMODE_NONE)
																					{
																						// TODO
																					}
																			#endif
																				}

																				void GetSurfaceAndBuiltinData(FragInputs fragInputs, float3 V, inout PositionInputs posInput, out SurfaceData surfaceData, out BuiltinData builtinData)
																				{
																					SurfaceDescriptionInputs surfaceDescriptionInputs = FragInputsToSurfaceDescriptionInputs(fragInputs, V);
																					SurfaceDescription surfaceDescription = SurfaceDescriptionFunction(surfaceDescriptionInputs);

																					// Perform alpha test very early to save performance (a killed pixel will not sample textures)
																					// TODO: split graph evaluation to grab just alpha dependencies first? tricky..
																					// DoAlphaTest(surfaceDescription.Alpha, surfaceDescription.AlphaClipThreshold);

																					BuildSurfaceData(fragInputs, surfaceDescription, V, posInput, surfaceData);

																					// Builtin Data
																					ZERO_INITIALIZE(BuiltinData, builtinData); // No call to InitBuiltinData as we don't have any lighting
																					builtinData.opacity = surfaceDescription.Alpha;
																				}

																				//-------------------------------------------------------------------------------------
																				// Pass Includes
																				//-------------------------------------------------------------------------------------
																					#include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/ShaderPassLightTransport.hlsl"
																				//-------------------------------------------------------------------------------------
																				// End Pass Includes
																				//-------------------------------------------------------------------------------------

																				ENDHLSL
																			}

																			Pass
																			{
																					// based on UnlitPass.template
																					Name "SceneSelectionPass"
																					Tags { "LightMode" = "SceneSelectionPass" }

																					//-------------------------------------------------------------------------------------
																					// Render Modes (Blend, Cull, ZTest, Stencil, etc)
																					//-------------------------------------------------------------------------------------



																					ZWrite Off


																					ColorMask 0

																					//-------------------------------------------------------------------------------------
																					// End Render Modes
																					//-------------------------------------------------------------------------------------

																					HLSLPROGRAM

																					#pragma target 4.5
																					#pragma only_renderers d3d11 ps4 xboxone vulkan metal switch
																					//#pragma enable_d3d11_debug_symbols

																					//enable GPU instancing support
																					#pragma multi_compile_instancing

																					//-------------------------------------------------------------------------------------
																					// Variant Definitions (active field translations to HDRP defines)
																					//-------------------------------------------------------------------------------------
																					#define _SURFACE_TYPE_TRANSPARENT 1
																					#define _BLENDMODE_ALPHA 1
																					// #define _BLENDMODE_ADD 1
																					// #define _BLENDMODE_PRE_MULTIPLY 1
																					// #define _ADD_PRECOMPUTED_VELOCITY

																					//-------------------------------------------------------------------------------------
																					// End Variant Definitions
																					//-------------------------------------------------------------------------------------

																					#pragma vertex Vert
																					#pragma fragment Frag

																					#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
																					#include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderVariables.hlsl"
																					#include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/FragInputs.hlsl"
																					#include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/ShaderPass.cs.hlsl"

																					//-------------------------------------------------------------------------------------
																					// Defines
																					//-------------------------------------------------------------------------------------
																							#define SHADERPASS SHADERPASS_DEPTH_ONLY
																						#define SCENESELECTIONPASS
																						#pragma editor_sync_compilation
																						// ACTIVE FIELDS:
																						//   SurfaceType.Transparent
																						//   BlendMode.Alpha
																						//   VertexDescriptionInputs.ObjectSpaceNormal
																						//   VertexDescriptionInputs.ObjectSpaceTangent
																						//   VertexDescriptionInputs.ObjectSpacePosition
																						//   SurfaceDescription.Alpha
																						//   SurfaceDescription.AlphaClipThreshold
																						//   features.modifyMesh
																						//   VertexDescription.VertexPosition
																						//   VertexDescription.VertexNormal
																						//   VertexDescription.VertexTangent
																						//   AttributesMesh.normalOS
																						//   AttributesMesh.tangentOS
																						//   AttributesMesh.positionOS
																						// Shared Graph Keywords

																					// this translates the new dependency tracker into the old preprocessor definitions for the existing HDRP shader code
																					#define ATTRIBUTES_NEED_NORMAL
																					#define ATTRIBUTES_NEED_TANGENT
																					// #define ATTRIBUTES_NEED_TEXCOORD0
																					// #define ATTRIBUTES_NEED_TEXCOORD1
																					// #define ATTRIBUTES_NEED_TEXCOORD2
																					// #define ATTRIBUTES_NEED_TEXCOORD3
																					// #define ATTRIBUTES_NEED_COLOR
																					// #define VARYINGS_NEED_POSITION_WS
																					// #define VARYINGS_NEED_TANGENT_TO_WORLD
																					// #define VARYINGS_NEED_TEXCOORD0
																					// #define VARYINGS_NEED_TEXCOORD1
																					// #define VARYINGS_NEED_TEXCOORD2
																					// #define VARYINGS_NEED_TEXCOORD3
																					// #define VARYINGS_NEED_COLOR
																					// #define VARYINGS_NEED_CULLFACE
																					#define HAVE_MESH_MODIFICATION

																					//-------------------------------------------------------------------------------------
																					// End Defines
																					//-------------------------------------------------------------------------------------


																					#include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/Material.hlsl"
																					#include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/Unlit/Unlit.hlsl"

																					#include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/BuiltinUtilities.hlsl"
																					#include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/MaterialUtilities.hlsl"
																					#include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderGraphFunctions.hlsl"

																					// Used by SceneSelectionPass
																					int _ObjectId;
																					int _PassValue;

																					//-------------------------------------------------------------------------------------
																					// Interpolator Packing And Struct Declarations
																					//-------------------------------------------------------------------------------------
																					// Generated Type: AttributesMesh
																					struct AttributesMesh
																					{
																						float3 positionOS : POSITION;
																						float3 normalOS : NORMAL; // optional
																						float4 tangentOS : TANGENT; // optional
																						#if UNITY_ANY_INSTANCING_ENABLED
																						uint instanceID : INSTANCEID_SEMANTIC;
																						#endif // UNITY_ANY_INSTANCING_ENABLED
																					};
																					// Generated Type: VaryingsMeshToPS
																					struct VaryingsMeshToPS
																					{
																						float4 positionCS : SV_POSITION;
																						#if UNITY_ANY_INSTANCING_ENABLED
																						uint instanceID : CUSTOM_INSTANCE_ID;
																						#endif // UNITY_ANY_INSTANCING_ENABLED
																						#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
																						FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
																						#endif // defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
																					};

																					// Generated Type: PackedVaryingsMeshToPS
																					struct PackedVaryingsMeshToPS
																					{
																						float4 positionCS : SV_POSITION; // unpacked
																						#if UNITY_ANY_INSTANCING_ENABLED
																						uint instanceID : CUSTOM_INSTANCE_ID; // unpacked
																						#endif // conditional
																						#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
																						FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC; // unpacked
																						#endif // conditional
																					};

																					// Packed Type: VaryingsMeshToPS
																					PackedVaryingsMeshToPS PackVaryingsMeshToPS(VaryingsMeshToPS input)
																					{
																						PackedVaryingsMeshToPS output = (PackedVaryingsMeshToPS)0;
																						output.positionCS = input.positionCS;
																						#if UNITY_ANY_INSTANCING_ENABLED
																						output.instanceID = input.instanceID;
																						#endif // conditional
																						#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
																						output.cullFace = input.cullFace;
																						#endif // conditional
																						return output;
																					}

																					// Unpacked Type: VaryingsMeshToPS
																					VaryingsMeshToPS UnpackVaryingsMeshToPS(PackedVaryingsMeshToPS input)
																					{
																						VaryingsMeshToPS output = (VaryingsMeshToPS)0;
																						output.positionCS = input.positionCS;
																						#if UNITY_ANY_INSTANCING_ENABLED
																						output.instanceID = input.instanceID;
																						#endif // conditional
																						#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
																						output.cullFace = input.cullFace;
																						#endif // conditional
																						return output;
																					}
																					// Generated Type: VaryingsMeshToDS
																					struct VaryingsMeshToDS
																					{
																						float3 positionRWS;
																						float3 normalWS;
																						#if UNITY_ANY_INSTANCING_ENABLED
																						uint instanceID : CUSTOM_INSTANCE_ID;
																						#endif // UNITY_ANY_INSTANCING_ENABLED
																					};

																					// Generated Type: PackedVaryingsMeshToDS
																					struct PackedVaryingsMeshToDS
																					{
																						#if UNITY_ANY_INSTANCING_ENABLED
																						uint instanceID : CUSTOM_INSTANCE_ID; // unpacked
																						#endif // conditional
																						float3 interp00 : TEXCOORD0; // auto-packed
																						float3 interp01 : TEXCOORD1; // auto-packed
																					};

																					// Packed Type: VaryingsMeshToDS
																					PackedVaryingsMeshToDS PackVaryingsMeshToDS(VaryingsMeshToDS input)
																					{
																						PackedVaryingsMeshToDS output = (PackedVaryingsMeshToDS)0;
																						output.interp00.xyz = input.positionRWS;
																						output.interp01.xyz = input.normalWS;
																						#if UNITY_ANY_INSTANCING_ENABLED
																						output.instanceID = input.instanceID;
																						#endif // conditional
																						return output;
																					}

																					// Unpacked Type: VaryingsMeshToDS
																					VaryingsMeshToDS UnpackVaryingsMeshToDS(PackedVaryingsMeshToDS input)
																					{
																						VaryingsMeshToDS output = (VaryingsMeshToDS)0;
																						output.positionRWS = input.interp00.xyz;
																						output.normalWS = input.interp01.xyz;
																						#if UNITY_ANY_INSTANCING_ENABLED
																						output.instanceID = input.instanceID;
																						#endif // conditional
																						return output;
																					}
																					//-------------------------------------------------------------------------------------
																					// End Interpolator Packing And Struct Declarations
																					//-------------------------------------------------------------------------------------

																					//-------------------------------------------------------------------------------------
																					// Graph generated code
																					//-------------------------------------------------------------------------------------
																							// Shared Graph Properties (uniform inputs)
																							CBUFFER_START(UnityPerMaterial)
																							float4 _OutlineColor;
																							float _ScaleFactor;
																							CBUFFER_END

																								// Vertex Graph Inputs
																									struct VertexDescriptionInputs
																									{
																										float3 ObjectSpaceNormal; // optional
																										float3 ObjectSpaceTangent; // optional
																										float3 ObjectSpacePosition; // optional
																									};
																							// Vertex Graph Outputs
																								struct VertexDescription
																								{
																									float3 VertexPosition;
																									float3 VertexNormal;
																									float3 VertexTangent;
																								};

																								// Pixel Graph Inputs
																									struct SurfaceDescriptionInputs
																									{
																									};
																									// Pixel Graph Outputs
																										struct SurfaceDescription
																										{
																											float Alpha;
																											float AlphaClipThreshold;
																										};

																										// Shared Graph Node Functions

																											void Unity_Multiply_float(float3 A, float3 B, out float3 Out)
																											{
																												Out = A * B;
																											}

																											// Vertex Graph Evaluation
																												VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
																												{
																													VertexDescription description = (VertexDescription)0;
																													float _Property_FE6E722B_Out_0 = _ScaleFactor;
																													float3 _Multiply_10848E78_Out_2;
																													Unity_Multiply_float((_Property_FE6E722B_Out_0.xxx), IN.ObjectSpacePosition, _Multiply_10848E78_Out_2);
																													description.VertexPosition = _Multiply_10848E78_Out_2;
																													description.VertexNormal = IN.ObjectSpaceNormal;
																													description.VertexTangent = IN.ObjectSpaceTangent;
																													return description;
																												}

																												// Pixel Graph Evaluation
																													SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
																													{
																														SurfaceDescription surface = (SurfaceDescription)0;
																														surface.Alpha = 1;
																														surface.AlphaClipThreshold = 0;
																														return surface;
																													}

																													//-------------------------------------------------------------------------------------
																													// End graph generated code
																													//-------------------------------------------------------------------------------------

																												//-------------------------------------------------------------------------------------
																													// TEMPLATE INCLUDE : VertexAnimation.template.hlsl
																													//-------------------------------------------------------------------------------------


																													VertexDescriptionInputs AttributesMeshToVertexDescriptionInputs(AttributesMesh input)
																													{
																														VertexDescriptionInputs output;
																														ZERO_INITIALIZE(VertexDescriptionInputs, output);

																														output.ObjectSpaceNormal = input.normalOS;
																														// output.WorldSpaceNormal =            TransformObjectToWorldNormal(input.normalOS);
																														// output.ViewSpaceNormal =             TransformWorldToViewDir(output.WorldSpaceNormal);
																														// output.TangentSpaceNormal =          float3(0.0f, 0.0f, 1.0f);
																														output.ObjectSpaceTangent = input.tangentOS;
																														// output.WorldSpaceTangent =           TransformObjectToWorldDir(input.tangentOS.xyz);
																														// output.ViewSpaceTangent =            TransformWorldToViewDir(output.WorldSpaceTangent);
																														// output.TangentSpaceTangent =         float3(1.0f, 0.0f, 0.0f);
																														// output.ObjectSpaceBiTangent =        normalize(cross(input.normalOS, input.tangentOS) * (input.tangentOS.w > 0.0f ? 1.0f : -1.0f) * GetOddNegativeScale());
																														// output.WorldSpaceBiTangent =         TransformObjectToWorldDir(output.ObjectSpaceBiTangent);
																														// output.ViewSpaceBiTangent =          TransformWorldToViewDir(output.WorldSpaceBiTangent);
																														// output.TangentSpaceBiTangent =       float3(0.0f, 1.0f, 0.0f);
																														output.ObjectSpacePosition = input.positionOS;
																														// output.WorldSpacePosition =          TransformObjectToWorld(input.positionOS);
																														// output.ViewSpacePosition =           TransformWorldToView(output.WorldSpacePosition);
																														// output.TangentSpacePosition =        float3(0.0f, 0.0f, 0.0f);
																														// output.AbsoluteWorldSpacePosition =  GetAbsolutePositionWS(TransformObjectToWorld(input.positionOS));
																														// output.WorldSpaceViewDirection =     GetWorldSpaceNormalizeViewDir(output.WorldSpacePosition);
																														// output.ObjectSpaceViewDirection =    TransformWorldToObjectDir(output.WorldSpaceViewDirection);
																														// output.ViewSpaceViewDirection =      TransformWorldToViewDir(output.WorldSpaceViewDirection);
																														// float3x3 tangentSpaceTransform =     float3x3(output.WorldSpaceTangent,output.WorldSpaceBiTangent,output.WorldSpaceNormal);
																														// output.TangentSpaceViewDirection =   mul(tangentSpaceTransform, output.WorldSpaceViewDirection);
																														// output.ScreenPosition =              ComputeScreenPos(TransformWorldToHClip(output.WorldSpacePosition), _ProjectionParams.x);
																														// output.uv0 =                         input.uv0;
																														// output.uv1 =                         input.uv1;
																														// output.uv2 =                         input.uv2;
																														// output.uv3 =                         input.uv3;
																														// output.VertexColor =                 input.color;
																														// output.BoneWeights =                 input.weights;
																														// output.BoneIndices =                 input.indices;

																														return output;
																													}

																													AttributesMesh ApplyMeshModification(AttributesMesh input, float3 timeParameters)
																													{
																														// build graph inputs
																														VertexDescriptionInputs vertexDescriptionInputs = AttributesMeshToVertexDescriptionInputs(input);
																														// Override time paramters with used one (This is required to correctly handle motion vector for vertex animation based on time)
																														// vertexDescriptionInputs.TimeParameters = timeParameters;

																														// evaluate vertex graph
																														VertexDescription vertexDescription = VertexDescriptionFunction(vertexDescriptionInputs);

																														// copy graph output to the results
																														input.positionOS = vertexDescription.VertexPosition;
																														input.normalOS = vertexDescription.VertexNormal;
																														input.tangentOS.xyz = vertexDescription.VertexTangent;

																														return input;
																													}

																													//-------------------------------------------------------------------------------------
																													// END TEMPLATE INCLUDE : VertexAnimation.template.hlsl
																													//-------------------------------------------------------------------------------------


																												//-------------------------------------------------------------------------------------
																													// TEMPLATE INCLUDE : SharedCode.template.hlsl
																													//-------------------------------------------------------------------------------------

																													#if !defined(SHADER_STAGE_RAY_TRACING)
																														FragInputs BuildFragInputs(VaryingsMeshToPS input)
																														{
																															FragInputs output;
																															ZERO_INITIALIZE(FragInputs, output);

																															// Init to some default value to make the computer quiet (else it output 'divide by zero' warning even if value is not used).
																															// TODO: this is a really poor workaround, but the variable is used in a bunch of places
																															// to compute normals which are then passed on elsewhere to compute other values...
																															output.tangentToWorld = k_identity3x3;
																															output.positionSS = input.positionCS;       // input.positionCS is SV_Position

																															// output.positionRWS = input.positionRWS;
																															// output.tangentToWorld = BuildTangentToWorld(input.tangentWS, input.normalWS);
																															// output.texCoord0 = input.texCoord0;
																															// output.texCoord1 = input.texCoord1;
																															// output.texCoord2 = input.texCoord2;
																															// output.texCoord3 = input.texCoord3;
																															// output.color = input.color;
																															#if _DOUBLESIDED_ON && SHADER_STAGE_FRAGMENT
																															output.isFrontFace = IS_FRONT_VFACE(input.cullFace, true, false);
																															#elif SHADER_STAGE_FRAGMENT
																															// output.isFrontFace = IS_FRONT_VFACE(input.cullFace, true, false);
																															#endif // SHADER_STAGE_FRAGMENT

																															return output;
																														}
																													#endif
																														SurfaceDescriptionInputs FragInputsToSurfaceDescriptionInputs(FragInputs input, float3 viewWS)
																														{
																															SurfaceDescriptionInputs output;
																															ZERO_INITIALIZE(SurfaceDescriptionInputs, output);

																															// output.WorldSpaceNormal =            input.tangentToWorld[2].xyz;	// normal was already normalized in BuildTangentToWorld()
																															// output.ObjectSpaceNormal =           normalize(mul(output.WorldSpaceNormal, (float3x3) UNITY_MATRIX_M));           // transposed multiplication by inverse matrix to handle normal scale
																															// output.ViewSpaceNormal =             mul(output.WorldSpaceNormal, (float3x3) UNITY_MATRIX_I_V);         // transposed multiplication by inverse matrix to handle normal scale
																															// output.TangentSpaceNormal =          float3(0.0f, 0.0f, 1.0f);
																															// output.WorldSpaceTangent =           input.tangentToWorld[0].xyz;
																															// output.ObjectSpaceTangent =          TransformWorldToObjectDir(output.WorldSpaceTangent);
																															// output.ViewSpaceTangent =            TransformWorldToViewDir(output.WorldSpaceTangent);
																															// output.TangentSpaceTangent =         float3(1.0f, 0.0f, 0.0f);
																															// output.WorldSpaceBiTangent =         input.tangentToWorld[1].xyz;
																															// output.ObjectSpaceBiTangent =        TransformWorldToObjectDir(output.WorldSpaceBiTangent);
																															// output.ViewSpaceBiTangent =          TransformWorldToViewDir(output.WorldSpaceBiTangent);
																															// output.TangentSpaceBiTangent =       float3(0.0f, 1.0f, 0.0f);
																															// output.WorldSpaceViewDirection =     normalize(viewWS);
																															// output.ObjectSpaceViewDirection =    TransformWorldToObjectDir(output.WorldSpaceViewDirection);
																															// output.ViewSpaceViewDirection =      TransformWorldToViewDir(output.WorldSpaceViewDirection);
																															// float3x3 tangentSpaceTransform =     float3x3(output.WorldSpaceTangent,output.WorldSpaceBiTangent,output.WorldSpaceNormal);
																															// output.TangentSpaceViewDirection =   mul(tangentSpaceTransform, output.WorldSpaceViewDirection);
																															// output.WorldSpacePosition =          input.positionRWS;
																															// output.ObjectSpacePosition =         TransformWorldToObject(input.positionRWS);
																															// output.ViewSpacePosition =           TransformWorldToView(input.positionRWS);
																															// output.TangentSpacePosition =        float3(0.0f, 0.0f, 0.0f);
																															// output.AbsoluteWorldSpacePosition =  GetAbsolutePositionWS(input.positionRWS);
																															// output.ScreenPosition =              ComputeScreenPos(TransformWorldToHClip(input.positionRWS), _ProjectionParams.x);
																															// output.uv0 =                         input.texCoord0;
																															// output.uv1 =                         input.texCoord1;
																															// output.uv2 =                         input.texCoord2;
																															// output.uv3 =                         input.texCoord3;
																															// output.VertexColor =                 input.color;
																															// output.FaceSign =                    input.isFrontFace;
																															// output.TimeParameters =              _TimeParameters.xyz; // This is mainly for LW as HD overwrite this value

																															return output;
																														}

																													#if !defined(SHADER_STAGE_RAY_TRACING)

																														// existing HDRP code uses the combined function to go directly from packed to frag inputs
																														FragInputs UnpackVaryingsMeshToFragInputs(PackedVaryingsMeshToPS input)
																														{
																															UNITY_SETUP_INSTANCE_ID(input);
																															VaryingsMeshToPS unpacked = UnpackVaryingsMeshToPS(input);
																															return BuildFragInputs(unpacked);
																														}
																													#endif

																														//-------------------------------------------------------------------------------------
																														// END TEMPLATE INCLUDE : SharedCode.template.hlsl
																														//-------------------------------------------------------------------------------------



																														void BuildSurfaceData(FragInputs fragInputs, inout SurfaceDescription surfaceDescription, float3 V, PositionInputs posInput, out SurfaceData surfaceData)
																														{
																															// setup defaults -- these are used if the graph doesn't output a value
																															ZERO_INITIALIZE(SurfaceData, surfaceData);

																															// copy across graph values, if defined
																															// surfaceData.color = surfaceDescription.Color;

																													#if defined(DEBUG_DISPLAY)
																															if (_DebugMipMapMode != DEBUGMIPMAPMODE_NONE)
																															{
																																// TODO
																															}
																													#endif
																														}

																														void GetSurfaceAndBuiltinData(FragInputs fragInputs, float3 V, inout PositionInputs posInput, out SurfaceData surfaceData, out BuiltinData builtinData)
																														{
																															SurfaceDescriptionInputs surfaceDescriptionInputs = FragInputsToSurfaceDescriptionInputs(fragInputs, V);
																															SurfaceDescription surfaceDescription = SurfaceDescriptionFunction(surfaceDescriptionInputs);

																															// Perform alpha test very early to save performance (a killed pixel will not sample textures)
																															// TODO: split graph evaluation to grab just alpha dependencies first? tricky..
																															// DoAlphaTest(surfaceDescription.Alpha, surfaceDescription.AlphaClipThreshold);

																															BuildSurfaceData(fragInputs, surfaceDescription, V, posInput, surfaceData);

																															// Builtin Data
																															ZERO_INITIALIZE(BuiltinData, builtinData); // No call to InitBuiltinData as we don't have any lighting
																															builtinData.opacity = surfaceDescription.Alpha;
																														}

																														//-------------------------------------------------------------------------------------
																														// Pass Includes
																														//-------------------------------------------------------------------------------------
																															#include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/ShaderPassDepthOnly.hlsl"
																														//-------------------------------------------------------------------------------------
																														// End Pass Includes
																														//-------------------------------------------------------------------------------------

																														ENDHLSL
																													}

																													Pass
																													{
																															// based on UnlitPass.template
																															Name "ForwardOnly"
																															Tags { "LightMode" = "ForwardOnly" }

																															//-------------------------------------------------------------------------------------
																															// Render Modes (Blend, Cull, ZTest, Stencil, etc)
																															//-------------------------------------------------------------------------------------
																															Blend One OneMinusSrcAlpha, One OneMinusSrcAlpha



																															ZWrite Off

																															// Stencil setup
																														Stencil
																														{
																														   WriteMask 6
																														   Ref  0
																														   Comp Always
																														   Pass Replace
																														}


																															//-------------------------------------------------------------------------------------
																															// End Render Modes
																															//-------------------------------------------------------------------------------------

																															HLSLPROGRAM

																															#pragma target 4.5
																															#pragma only_renderers d3d11 ps4 xboxone vulkan metal switch
																															//#pragma enable_d3d11_debug_symbols

																															//enable GPU instancing support
																															#pragma multi_compile_instancing

																															//-------------------------------------------------------------------------------------
																															// Variant Definitions (active field translations to HDRP defines)
																															//-------------------------------------------------------------------------------------
																															#define _SURFACE_TYPE_TRANSPARENT 1
																															#define _BLENDMODE_ALPHA 1
																															// #define _BLENDMODE_ADD 1
																															// #define _BLENDMODE_PRE_MULTIPLY 1
																															// #define _ADD_PRECOMPUTED_VELOCITY

																															//-------------------------------------------------------------------------------------
																															// End Variant Definitions
																															//-------------------------------------------------------------------------------------

																															#pragma vertex Vert
																															#pragma fragment Frag

																															#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
																															#include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderVariables.hlsl"
																															#include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/FragInputs.hlsl"
																															#include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/ShaderPass.cs.hlsl"

																															//-------------------------------------------------------------------------------------
																															// Defines
																															//-------------------------------------------------------------------------------------
																																	#define SHADERPASS SHADERPASS_FORWARD_UNLIT
																																#pragma multi_compile _ DEBUG_DISPLAY
																																// ACTIVE FIELDS:
																																//   SurfaceType.Transparent
																																//   BlendMode.Alpha
																																//   VertexDescriptionInputs.ObjectSpaceNormal
																																//   VertexDescriptionInputs.ObjectSpaceTangent
																																//   VertexDescriptionInputs.ObjectSpacePosition
																																//   SurfaceDescription.Color
																																//   SurfaceDescription.Alpha
																																//   SurfaceDescription.AlphaClipThreshold
																																//   features.modifyMesh
																																//   VertexDescription.VertexPosition
																																//   VertexDescription.VertexNormal
																																//   VertexDescription.VertexTangent
																																//   AttributesMesh.normalOS
																																//   AttributesMesh.tangentOS
																																//   AttributesMesh.positionOS
																																// Shared Graph Keywords

																															// this translates the new dependency tracker into the old preprocessor definitions for the existing HDRP shader code
																															#define ATTRIBUTES_NEED_NORMAL
																															#define ATTRIBUTES_NEED_TANGENT
																															// #define ATTRIBUTES_NEED_TEXCOORD0
																															// #define ATTRIBUTES_NEED_TEXCOORD1
																															// #define ATTRIBUTES_NEED_TEXCOORD2
																															// #define ATTRIBUTES_NEED_TEXCOORD3
																															// #define ATTRIBUTES_NEED_COLOR
																															// #define VARYINGS_NEED_POSITION_WS
																															// #define VARYINGS_NEED_TANGENT_TO_WORLD
																															// #define VARYINGS_NEED_TEXCOORD0
																															// #define VARYINGS_NEED_TEXCOORD1
																															// #define VARYINGS_NEED_TEXCOORD2
																															// #define VARYINGS_NEED_TEXCOORD3
																															// #define VARYINGS_NEED_COLOR
																															// #define VARYINGS_NEED_CULLFACE
																															#define HAVE_MESH_MODIFICATION

																															//-------------------------------------------------------------------------------------
																															// End Defines
																															//-------------------------------------------------------------------------------------


																															#include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/Material.hlsl"
																															#include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/Unlit/Unlit.hlsl"

																															#include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/BuiltinUtilities.hlsl"
																															#include "Packages/com.unity.render-pipelines.high-definition/Runtime/Material/MaterialUtilities.hlsl"
																															#include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderGraphFunctions.hlsl"

																															// Used by SceneSelectionPass
																															int _ObjectId;
																															int _PassValue;

																															//-------------------------------------------------------------------------------------
																															// Interpolator Packing And Struct Declarations
																															//-------------------------------------------------------------------------------------
																															// Generated Type: AttributesMesh
																															struct AttributesMesh
																															{
																																float3 positionOS : POSITION;
																																float3 normalOS : NORMAL; // optional
																																float4 tangentOS : TANGENT; // optional
																																#if UNITY_ANY_INSTANCING_ENABLED
																																uint instanceID : INSTANCEID_SEMANTIC;
																																#endif // UNITY_ANY_INSTANCING_ENABLED
																															};
																															// Generated Type: VaryingsMeshToPS
																															struct VaryingsMeshToPS
																															{
																																float4 positionCS : SV_POSITION;
																																#if UNITY_ANY_INSTANCING_ENABLED
																																uint instanceID : CUSTOM_INSTANCE_ID;
																																#endif // UNITY_ANY_INSTANCING_ENABLED
																																#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
																																FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
																																#endif // defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
																															};

																															// Generated Type: PackedVaryingsMeshToPS
																															struct PackedVaryingsMeshToPS
																															{
																																float4 positionCS : SV_POSITION; // unpacked
																																#if UNITY_ANY_INSTANCING_ENABLED
																																uint instanceID : CUSTOM_INSTANCE_ID; // unpacked
																																#endif // conditional
																																#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
																																FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC; // unpacked
																																#endif // conditional
																															};

																															// Packed Type: VaryingsMeshToPS
																															PackedVaryingsMeshToPS PackVaryingsMeshToPS(VaryingsMeshToPS input)
																															{
																																PackedVaryingsMeshToPS output = (PackedVaryingsMeshToPS)0;
																																output.positionCS = input.positionCS;
																																#if UNITY_ANY_INSTANCING_ENABLED
																																output.instanceID = input.instanceID;
																																#endif // conditional
																																#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
																																output.cullFace = input.cullFace;
																																#endif // conditional
																																return output;
																															}

																															// Unpacked Type: VaryingsMeshToPS
																															VaryingsMeshToPS UnpackVaryingsMeshToPS(PackedVaryingsMeshToPS input)
																															{
																																VaryingsMeshToPS output = (VaryingsMeshToPS)0;
																																output.positionCS = input.positionCS;
																																#if UNITY_ANY_INSTANCING_ENABLED
																																output.instanceID = input.instanceID;
																																#endif // conditional
																																#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
																																output.cullFace = input.cullFace;
																																#endif // conditional
																																return output;
																															}
																															// Generated Type: VaryingsMeshToDS
																															struct VaryingsMeshToDS
																															{
																																float3 positionRWS;
																																float3 normalWS;
																																#if UNITY_ANY_INSTANCING_ENABLED
																																uint instanceID : CUSTOM_INSTANCE_ID;
																																#endif // UNITY_ANY_INSTANCING_ENABLED
																															};

																															// Generated Type: PackedVaryingsMeshToDS
																															struct PackedVaryingsMeshToDS
																															{
																																#if UNITY_ANY_INSTANCING_ENABLED
																																uint instanceID : CUSTOM_INSTANCE_ID; // unpacked
																																#endif // conditional
																																float3 interp00 : TEXCOORD0; // auto-packed
																																float3 interp01 : TEXCOORD1; // auto-packed
																															};

																															// Packed Type: VaryingsMeshToDS
																															PackedVaryingsMeshToDS PackVaryingsMeshToDS(VaryingsMeshToDS input)
																															{
																																PackedVaryingsMeshToDS output = (PackedVaryingsMeshToDS)0;
																																output.interp00.xyz = input.positionRWS;
																																output.interp01.xyz = input.normalWS;
																																#if UNITY_ANY_INSTANCING_ENABLED
																																output.instanceID = input.instanceID;
																																#endif // conditional
																																return output;
																															}

																															// Unpacked Type: VaryingsMeshToDS
																															VaryingsMeshToDS UnpackVaryingsMeshToDS(PackedVaryingsMeshToDS input)
																															{
																																VaryingsMeshToDS output = (VaryingsMeshToDS)0;
																																output.positionRWS = input.interp00.xyz;
																																output.normalWS = input.interp01.xyz;
																																#if UNITY_ANY_INSTANCING_ENABLED
																																output.instanceID = input.instanceID;
																																#endif // conditional
																																return output;
																															}
																															//-------------------------------------------------------------------------------------
																															// End Interpolator Packing And Struct Declarations
																															//-------------------------------------------------------------------------------------

																															//-------------------------------------------------------------------------------------
																															// Graph generated code
																															//-------------------------------------------------------------------------------------
																																	// Shared Graph Properties (uniform inputs)
																																	CBUFFER_START(UnityPerMaterial)
																																	float4 _OutlineColor;
																																	float _ScaleFactor;
																																	CBUFFER_END

																																		// Vertex Graph Inputs
																																			struct VertexDescriptionInputs
																																			{
																																				float3 ObjectSpaceNormal; // optional
																																				float3 ObjectSpaceTangent; // optional
																																				float3 ObjectSpacePosition; // optional
																																			};
																																	// Vertex Graph Outputs
																																		struct VertexDescription
																																		{
																																			float3 VertexPosition;
																																			float3 VertexNormal;
																																			float3 VertexTangent;
																																		};

																																		// Pixel Graph Inputs
																																			struct SurfaceDescriptionInputs
																																			{
																																			};
																																			// Pixel Graph Outputs
																																				struct SurfaceDescription
																																				{
																																					float3 Color;
																																					float Alpha;
																																					float AlphaClipThreshold;
																																				};

																																				// Shared Graph Node Functions

																																					void Unity_Multiply_float(float3 A, float3 B, out float3 Out)
																																					{
																																						Out = A * B;
																																					}

																																					// Vertex Graph Evaluation
																																						VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
																																						{
																																							VertexDescription description = (VertexDescription)0;
																																							float _Property_FE6E722B_Out_0 = _ScaleFactor;
																																							float3 _Multiply_10848E78_Out_2;
																																							Unity_Multiply_float((_Property_FE6E722B_Out_0.xxx), IN.ObjectSpacePosition, _Multiply_10848E78_Out_2);
																																							description.VertexPosition = _Multiply_10848E78_Out_2;
																																							description.VertexNormal = IN.ObjectSpaceNormal;
																																							description.VertexTangent = IN.ObjectSpaceTangent;
																																							return description;
																																						}

																																						// Pixel Graph Evaluation
																																							SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
																																							{
																																								SurfaceDescription surface = (SurfaceDescription)0;
																																								float4 _Property_C8A34B75_Out_0 = _OutlineColor;
																																								surface.Color = (_Property_C8A34B75_Out_0.xyz);
																																								surface.Alpha = 1;
																																								surface.AlphaClipThreshold = 0;
																																								return surface;
																																							}

																																							//-------------------------------------------------------------------------------------
																																							// End graph generated code
																																							//-------------------------------------------------------------------------------------

																																						//-------------------------------------------------------------------------------------
																																							// TEMPLATE INCLUDE : VertexAnimation.template.hlsl
																																							//-------------------------------------------------------------------------------------


																																							VertexDescriptionInputs AttributesMeshToVertexDescriptionInputs(AttributesMesh input)
																																							{
																																								VertexDescriptionInputs output;
																																								ZERO_INITIALIZE(VertexDescriptionInputs, output);

																																								output.ObjectSpaceNormal = input.normalOS;
																																								// output.WorldSpaceNormal =            TransformObjectToWorldNormal(input.normalOS);
																																								// output.ViewSpaceNormal =             TransformWorldToViewDir(output.WorldSpaceNormal);
																																								// output.TangentSpaceNormal =          float3(0.0f, 0.0f, 1.0f);
																																								output.ObjectSpaceTangent = input.tangentOS;
																																								// output.WorldSpaceTangent =           TransformObjectToWorldDir(input.tangentOS.xyz);
																																								// output.ViewSpaceTangent =            TransformWorldToViewDir(output.WorldSpaceTangent);
																																								// output.TangentSpaceTangent =         float3(1.0f, 0.0f, 0.0f);
																																								// output.ObjectSpaceBiTangent =        normalize(cross(input.normalOS, input.tangentOS) * (input.tangentOS.w > 0.0f ? 1.0f : -1.0f) * GetOddNegativeScale());
																																								// output.WorldSpaceBiTangent =         TransformObjectToWorldDir(output.ObjectSpaceBiTangent);
																																								// output.ViewSpaceBiTangent =          TransformWorldToViewDir(output.WorldSpaceBiTangent);
																																								// output.TangentSpaceBiTangent =       float3(0.0f, 1.0f, 0.0f);
																																								output.ObjectSpacePosition = input.positionOS;
																																								// output.WorldSpacePosition =          TransformObjectToWorld(input.positionOS);
																																								// output.ViewSpacePosition =           TransformWorldToView(output.WorldSpacePosition);
																																								// output.TangentSpacePosition =        float3(0.0f, 0.0f, 0.0f);
																																								// output.AbsoluteWorldSpacePosition =  GetAbsolutePositionWS(TransformObjectToWorld(input.positionOS));
																																								// output.WorldSpaceViewDirection =     GetWorldSpaceNormalizeViewDir(output.WorldSpacePosition);
																																								// output.ObjectSpaceViewDirection =    TransformWorldToObjectDir(output.WorldSpaceViewDirection);
																																								// output.ViewSpaceViewDirection =      TransformWorldToViewDir(output.WorldSpaceViewDirection);
																																								// float3x3 tangentSpaceTransform =     float3x3(output.WorldSpaceTangent,output.WorldSpaceBiTangent,output.WorldSpaceNormal);
																																								// output.TangentSpaceViewDirection =   mul(tangentSpaceTransform, output.WorldSpaceViewDirection);
																																								// output.ScreenPosition =              ComputeScreenPos(TransformWorldToHClip(output.WorldSpacePosition), _ProjectionParams.x);
																																								// output.uv0 =                         input.uv0;
																																								// output.uv1 =                         input.uv1;
																																								// output.uv2 =                         input.uv2;
																																								// output.uv3 =                         input.uv3;
																																								// output.VertexColor =                 input.color;
																																								// output.BoneWeights =                 input.weights;
																																								// output.BoneIndices =                 input.indices;

																																								return output;
																																							}

																																							AttributesMesh ApplyMeshModification(AttributesMesh input, float3 timeParameters)
																																							{
																																								// build graph inputs
																																								VertexDescriptionInputs vertexDescriptionInputs = AttributesMeshToVertexDescriptionInputs(input);
																																								// Override time paramters with used one (This is required to correctly handle motion vector for vertex animation based on time)
																																								// vertexDescriptionInputs.TimeParameters = timeParameters;

																																								// evaluate vertex graph
																																								VertexDescription vertexDescription = VertexDescriptionFunction(vertexDescriptionInputs);

																																								// copy graph output to the results
																																								input.positionOS = vertexDescription.VertexPosition;
																																								input.normalOS = vertexDescription.VertexNormal;
																																								input.tangentOS.xyz = vertexDescription.VertexTangent;

																																								return input;
																																							}

																																							//-------------------------------------------------------------------------------------
																																							// END TEMPLATE INCLUDE : VertexAnimation.template.hlsl
																																							//-------------------------------------------------------------------------------------


																																						//-------------------------------------------------------------------------------------
																																							// TEMPLATE INCLUDE : SharedCode.template.hlsl
																																							//-------------------------------------------------------------------------------------

																																							#if !defined(SHADER_STAGE_RAY_TRACING)
																																								FragInputs BuildFragInputs(VaryingsMeshToPS input)
																																								{
																																									FragInputs output;
																																									ZERO_INITIALIZE(FragInputs, output);

																																									// Init to some default value to make the computer quiet (else it output 'divide by zero' warning even if value is not used).
																																									// TODO: this is a really poor workaround, but the variable is used in a bunch of places
																																									// to compute normals which are then passed on elsewhere to compute other values...
																																									output.tangentToWorld = k_identity3x3;
																																									output.positionSS = input.positionCS;       // input.positionCS is SV_Position

																																									// output.positionRWS = input.positionRWS;
																																									// output.tangentToWorld = BuildTangentToWorld(input.tangentWS, input.normalWS);
																																									// output.texCoord0 = input.texCoord0;
																																									// output.texCoord1 = input.texCoord1;
																																									// output.texCoord2 = input.texCoord2;
																																									// output.texCoord3 = input.texCoord3;
																																									// output.color = input.color;
																																									#if _DOUBLESIDED_ON && SHADER_STAGE_FRAGMENT
																																									output.isFrontFace = IS_FRONT_VFACE(input.cullFace, true, false);
																																									#elif SHADER_STAGE_FRAGMENT
																																									// output.isFrontFace = IS_FRONT_VFACE(input.cullFace, true, false);
																																									#endif // SHADER_STAGE_FRAGMENT

																																									return output;
																																								}
																																							#endif
																																								SurfaceDescriptionInputs FragInputsToSurfaceDescriptionInputs(FragInputs input, float3 viewWS)
																																								{
																																									SurfaceDescriptionInputs output;
																																									ZERO_INITIALIZE(SurfaceDescriptionInputs, output);

																																									// output.WorldSpaceNormal =            input.tangentToWorld[2].xyz;	// normal was already normalized in BuildTangentToWorld()
																																									// output.ObjectSpaceNormal =           normalize(mul(output.WorldSpaceNormal, (float3x3) UNITY_MATRIX_M));           // transposed multiplication by inverse matrix to handle normal scale
																																									// output.ViewSpaceNormal =             mul(output.WorldSpaceNormal, (float3x3) UNITY_MATRIX_I_V);         // transposed multiplication by inverse matrix to handle normal scale
																																									// output.TangentSpaceNormal =          float3(0.0f, 0.0f, 1.0f);
																																									// output.WorldSpaceTangent =           input.tangentToWorld[0].xyz;
																																									// output.ObjectSpaceTangent =          TransformWorldToObjectDir(output.WorldSpaceTangent);
																																									// output.ViewSpaceTangent =            TransformWorldToViewDir(output.WorldSpaceTangent);
																																									// output.TangentSpaceTangent =         float3(1.0f, 0.0f, 0.0f);
																																									// output.WorldSpaceBiTangent =         input.tangentToWorld[1].xyz;
																																									// output.ObjectSpaceBiTangent =        TransformWorldToObjectDir(output.WorldSpaceBiTangent);
																																									// output.ViewSpaceBiTangent =          TransformWorldToViewDir(output.WorldSpaceBiTangent);
																																									// output.TangentSpaceBiTangent =       float3(0.0f, 1.0f, 0.0f);
																																									// output.WorldSpaceViewDirection =     normalize(viewWS);
																																									// output.ObjectSpaceViewDirection =    TransformWorldToObjectDir(output.WorldSpaceViewDirection);
																																									// output.ViewSpaceViewDirection =      TransformWorldToViewDir(output.WorldSpaceViewDirection);
																																									// float3x3 tangentSpaceTransform =     float3x3(output.WorldSpaceTangent,output.WorldSpaceBiTangent,output.WorldSpaceNormal);
																																									// output.TangentSpaceViewDirection =   mul(tangentSpaceTransform, output.WorldSpaceViewDirection);
																																									// output.WorldSpacePosition =          input.positionRWS;
																																									// output.ObjectSpacePosition =         TransformWorldToObject(input.positionRWS);
																																									// output.ViewSpacePosition =           TransformWorldToView(input.positionRWS);
																																									// output.TangentSpacePosition =        float3(0.0f, 0.0f, 0.0f);
																																									// output.AbsoluteWorldSpacePosition =  GetAbsolutePositionWS(input.positionRWS);
																																									// output.ScreenPosition =              ComputeScreenPos(TransformWorldToHClip(input.positionRWS), _ProjectionParams.x);
																																									// output.uv0 =                         input.texCoord0;
																																									// output.uv1 =                         input.texCoord1;
																																									// output.uv2 =                         input.texCoord2;
																																									// output.uv3 =                         input.texCoord3;
																																									// output.VertexColor =                 input.color;
																																									// output.FaceSign =                    input.isFrontFace;
																																									// output.TimeParameters =              _TimeParameters.xyz; // This is mainly for LW as HD overwrite this value

																																									return output;
																																								}

																																							#if !defined(SHADER_STAGE_RAY_TRACING)

																																								// existing HDRP code uses the combined function to go directly from packed to frag inputs
																																								FragInputs UnpackVaryingsMeshToFragInputs(PackedVaryingsMeshToPS input)
																																								{
																																									UNITY_SETUP_INSTANCE_ID(input);
																																									VaryingsMeshToPS unpacked = UnpackVaryingsMeshToPS(input);
																																									return BuildFragInputs(unpacked);
																																								}
																																							#endif

																																								//-------------------------------------------------------------------------------------
																																								// END TEMPLATE INCLUDE : SharedCode.template.hlsl
																																								//-------------------------------------------------------------------------------------



																																								void BuildSurfaceData(FragInputs fragInputs, inout SurfaceDescription surfaceDescription, float3 V, PositionInputs posInput, out SurfaceData surfaceData)
																																								{
																																									// setup defaults -- these are used if the graph doesn't output a value
																																									ZERO_INITIALIZE(SurfaceData, surfaceData);

																																									// copy across graph values, if defined
																																									surfaceData.color = surfaceDescription.Color;

																																							#if defined(DEBUG_DISPLAY)
																																									if (_DebugMipMapMode != DEBUGMIPMAPMODE_NONE)
																																									{
																																										// TODO
																																									}
																																							#endif
																																								}

																																								void GetSurfaceAndBuiltinData(FragInputs fragInputs, float3 V, inout PositionInputs posInput, out SurfaceData surfaceData, out BuiltinData builtinData)
																																								{
																																									SurfaceDescriptionInputs surfaceDescriptionInputs = FragInputsToSurfaceDescriptionInputs(fragInputs, V);
																																									SurfaceDescription surfaceDescription = SurfaceDescriptionFunction(surfaceDescriptionInputs);

																																									// Perform alpha test very early to save performance (a killed pixel will not sample textures)
																																									// TODO: split graph evaluation to grab just alpha dependencies first? tricky..
																																									// DoAlphaTest(surfaceDescription.Alpha, surfaceDescription.AlphaClipThreshold);

																																									BuildSurfaceData(fragInputs, surfaceDescription, V, posInput, surfaceData);

																																									// Builtin Data
																																									ZERO_INITIALIZE(BuiltinData, builtinData); // No call to InitBuiltinData as we don't have any lighting
																																									builtinData.opacity = surfaceDescription.Alpha;
																																								}

																																								//-------------------------------------------------------------------------------------
																																								// Pass Includes
																																								//-------------------------------------------------------------------------------------
																																									#include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/ShaderPass/ShaderPassForwardUnlit.hlsl"
																																								//-------------------------------------------------------------------------------------
																																								// End Pass Includes
																																								//-------------------------------------------------------------------------------------

																																								ENDHLSL
																																							}

	}
		CustomEditor "UnityEditor.Rendering.HighDefinition.UnlitUI"
																																									FallBack "Hidden/Shader Graph/FallbackError"
}

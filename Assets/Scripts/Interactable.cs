﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody)), 
    RequireComponent(typeof(Collider)), 
    RequireComponent(typeof(OutlineScript))
]
public class Interactable : MonoBehaviour
{
    public Rigidbody m_rigidbody;
    public Collider m_collider;
    public OutlineScript m_outline;
    public float smoothSpeed = 0.5f;

    [HideInInspector] public bool m_cannotBeDropped = false;

    /// <summary>
    /// The position vector - Choose which axis of the position should be followed
    /// </summary>
    public enum PositionVector { XYZ, X, Y, Z, XY, XZ, YZ };
    public PositionVector positionVector = PositionVector.XYZ;

    public Transform attachedPoint
    {
        get { return m_attachedPoint; }
        set { m_attachedPoint = value;
            UpdateAttachmentParams();
        }
    }
    private Transform m_attachedPoint;
    private Vector3 desiredPosition = Vector3.zero;

    public bool isHovered
    {
        get { return m_isHovered; }
        set { m_isHovered = value;
            UpdateOutlineSystem();
        }
    }
    [SerializeField] private bool m_isHovered= false;
    public bool isSelected = false;

    private void Awake()
    {
        m_rigidbody = GetComponent<Rigidbody>();
        m_collider = GetComponent<Collider>();
        m_outline = GetComponent<OutlineScript>();
    }

    void LateUpdate()
    {
        if (m_attachedPoint)
        {
            SetPositionTransform();
        }
    }

    public void UpdateOutlineSystem()
    {
        if (!m_outline.outlineRenderer)
        {
            m_outline.outlineRenderer = m_outline.CreateOutline();
        }
        m_outline.outlineRenderer.enabled = m_isHovered;
        this.GetComponent<Renderer>().enabled = !m_isHovered;
    }

    public void UpdateAttachmentParams()
    {
        if (isSelected)
        {
            m_rigidbody.useGravity = false;
            m_collider.isTrigger = true;
            m_outline.SetNewColor(m_outline.CanPlaceOutlineMat);
        }
        else
        {
            m_rigidbody.useGravity = true;
            m_collider.isTrigger = false;
            m_outline.SetNewColor(m_outline.HighlightOutlineMat);
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.layer == LayerMask.NameToLayer("Props")) // props layer
        {
            m_outline.SetNewColor(m_outline.CannotPlaceOutlineMat);
            m_cannotBeDropped = true;
        }
    }

    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.layer == LayerMask.NameToLayer("Props")) // props layer
        {
            m_outline.SetNewColor(m_outline.CanPlaceOutlineMat);
            m_cannotBeDropped = false;
        }
    }

    public void Delete()
    {
        Destroy(this.gameObject);
    }

    void SetPositionTransform()
    {
        
        // Position 
        switch (positionVector)
        {
            case PositionVector.XYZ:
                desiredPosition = new Vector3(m_attachedPoint.position.x, m_attachedPoint.position.y, m_attachedPoint.position.z);
                break;
            case PositionVector.X:
                desiredPosition = new Vector3(m_attachedPoint.position.x, transform.position.y, transform.position.z);
                break;
            case PositionVector.Y:
                desiredPosition = new Vector3(transform.position.x, m_attachedPoint.position.y, transform.position.z);
                break;
            case PositionVector.Z:
                desiredPosition = new Vector3(transform.position.x, transform.position.y, m_attachedPoint.position.z);
                break;
            case PositionVector.XY:
                desiredPosition = new Vector3(m_attachedPoint.position.x, m_attachedPoint.position.y, transform.position.z);
                break;
            case PositionVector.XZ:
                desiredPosition = new Vector3(m_attachedPoint.position.x, transform.position.y, m_attachedPoint.position.z);
                break;
            case PositionVector.YZ:
                desiredPosition = new Vector3(transform.position.x, m_attachedPoint.position.y, m_attachedPoint.position.z);
                break;
            default:
                print("Incorrect Position Vector");
                break;
        }

        transform.position = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed * Time.smoothDeltaTime);
    }
}

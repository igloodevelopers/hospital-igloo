﻿using UnityEngine;

public class OutlineScript : MonoBehaviour
{
    public Material CanPlaceOutlineMat = null;
    public Material CannotPlaceOutlineMat = null;
    public Material HighlightOutlineMat = null;
    [HideInInspector] public Renderer outlineRenderer;

    private Material[] mats;

    void Start()
    {
        if(CannotPlaceOutlineMat == null || CanPlaceOutlineMat == null || HighlightOutlineMat == null)
        {
            throw new System.Exception($"Not enough materials assigned to OutlineScript on {this.gameObject.name}");
        }
        if(!outlineRenderer)
            outlineRenderer = CreateOutline();
    }

    public Renderer CreateOutline()
    {
        // Make a cube, put it in the right place, with the correct scale,etc.
        GameObject outlineObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
        outlineObject.GetComponent<Collider>().enabled = false;
        outlineObject.transform.SetParent(transform);
        outlineObject.transform.position = transform.position;
        outlineObject.transform.rotation = transform.rotation;
        outlineObject.transform.localScale = transform.localScale;
        

        // Grab the meshfilter and add the correct mesh to copy the one we are using.
        outlineObject.GetComponent<MeshFilter>().mesh = this.GetComponent<MeshFilter>().mesh;

        // Grab the renderer, and make it the same amount of materials as parent.
        Renderer rend = outlineObject.GetComponent<Renderer>();

        // Make a new array of materials
        mats = new Material[this.GetComponent<Renderer>().materials.Length];
        // loop through all materials, and set them all to be 
        var newMat = new Material(HighlightOutlineMat);
        for (int i = 0; i < mats.Length; i++)
        {
            mats[i] = newMat;
        }
        rend.materials = mats;

        // Turn the render off, after setting the shadows correctly.
        rend.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        rend.enabled = false;

        return rend;
    }

    public void SetNewColor(Material mat)
    {
        if (outlineRenderer == null) return; // just encase

        // loop through all materials, and set them all to be 
        var newMat = new Material(mat);

        for (int i = 0; i < mats.Length; i++)
        {
            mats[i] = newMat;
        }
        outlineRenderer.materials = mats;
    }
}
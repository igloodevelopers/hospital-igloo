﻿using UnityEngine;
using UnityEngine.UI;

public class UIControlsWidget : MonoBehaviour
{

    [SerializeField, Header("Button Widgets")] private Image m_xButton;
    [SerializeField] private Image m_yButton;
    [SerializeField] private Image m_bButton;
    [SerializeField] private Image m_aButton;

    [SerializeField, Header("Text Widgets")] private Text m_xText;
    [SerializeField] private Text m_yText;
    [SerializeField] private Text m_bText;
    [SerializeField] private Text m_aText;

    [SerializeField, Header("Colours")] private Color m_origIconColor;
    [SerializeField] private Color m_unusedIconColor;

    private bool m_isVisible = true;

    private static UIControlsWidget instance;
    public static UIControlsWidget Instance
    {
        get
        {
            if (instance == null) instance = FindObjectOfType<UIControlsWidget>();
            return instance;
        }
    }

    public enum CurrentSelectionStatus
    {
        OnNothing, OnItem, OnMenu
    }
    private CurrentSelectionStatus currentStatus = CurrentSelectionStatus.OnItem;

    public void ToggleVisibility(bool newVisibility)
    {
        m_isVisible = newVisibility;
        if (!m_isVisible)
        {
            m_xText.text = "";
            m_xButton.color = Color.clear;

            // B button
            m_bText.text = "";
            m_bButton.color = Color.clear;

            // A Button
            m_aButton.color = Color.clear;
            m_aText.text = "";

            // Y Button
            m_yButton.color = Color.clear;
            m_yText.text = "";
        }
        else
        {
            UpdateCurrentSelectionStatus(currentStatus);
        }
    }

    public void Start()
    {
        UpdateCurrentSelectionStatus(CurrentSelectionStatus.OnNothing);
    }

    /// <summary>
    /// Called from PlayerPointer, and uses a switch to setup the various icons and text fields
    /// so that the correct information is shown to the player.
    /// </summary>
    /// <param name="newStatus"></param>
    public void UpdateCurrentSelectionStatus(CurrentSelectionStatus newStatus)
    {
        currentStatus = newStatus; // keep the new status cached encase it's turned visible again.
        if (!m_isVisible) return; // don't adjust stuff if we're hiding the menu.
        
        switch (newStatus)
        {
            case CurrentSelectionStatus.OnNothing:
                // X button
                m_xText.text = "";
                m_xButton.color = m_unusedIconColor;

                // B button
                m_bText.text = "";
                m_bButton.color = m_unusedIconColor;

                // A Button
                m_aButton.color = m_origIconColor;
                m_aText.text = "SELECT";

                // Y Button
                m_yButton.color = m_origIconColor;
                m_yText.text = "MENU";
                break;
            case CurrentSelectionStatus.OnItem:
                // X Button
                m_xText.text = "ROTATE L";
                m_xButton.color = m_origIconColor;

                // B Button
                m_bText.text = "ROTATE R";
                m_bButton.color = m_origIconColor;

                // A Button
                m_aButton.color = m_origIconColor;
                m_aText.text = "DROP";

                // Y Button
                m_yButton.color = m_origIconColor;
                m_yText.text = "DELETE";
                break;
            case CurrentSelectionStatus.OnMenu:

                // X Button // Possibly switch pages:??
                m_xText.text = "";
                m_xButton.color = m_unusedIconColor;

                // B Button // Possibly switch pages:??
                m_bText.text = "";
                m_bButton.color = m_unusedIconColor;

                // A Button
                m_aButton.color = m_origIconColor;
                m_aText.text = "SELECT";

                // Y Button
                m_yButton.color = m_origIconColor;
                m_yText.text = "CLOSE";
                break;
;
        }
    }
}

﻿using UnityEngine;
using Igloo;

public class TeleportController : MonoBehaviour
{
    private enum CurrentLocation { Ward, Reception };
    private CurrentLocation curLocation = CurrentLocation.Ward;

    private readonly string Teleport1LocationName = "Scene1_Teleport";
    private readonly string Teleport2LocationName = "Scene2_Teleport";

    public Transform TeleportLocation1 = null;
    public Transform TeleportLocation2 = null;

    private PlayerManager playerManager = null;

    public GameObject Scene1Root;
    private Renderer[] scene1Renderers;
    public GameObject Scene2Root;
    private Renderer[] scene2Renderers;

    public Transform[] Scene1Lights;
    public Transform[] Scene2Lights;

    private void Start()
    {
        scene1Renderers = Scene1Root.GetComponentsInChildren<MeshRenderer>();
        scene2Renderers = Scene2Root.GetComponentsInChildren<MeshRenderer>();
        ToggleSceneVisibility();
    }

    private void OnTriggerEnter(Collider col)
    {
        CheckPlayerManager();

        if (col.gameObject.name == Teleport1LocationName)
        {
            playerManager.TeleportPlayer(TeleportLocation2.position, TeleportLocation2.rotation, 1.15f);
            curLocation = CurrentLocation.Reception;
            ToggleSceneVisibility();
        }
        else if(col.gameObject.name == Teleport2LocationName)
        {
            playerManager.TeleportPlayer(TeleportLocation1.position, TeleportLocation1.rotation, 2f);
            curLocation = CurrentLocation.Ward;
            ToggleSceneVisibility();
        }

    }

    private void CheckPlayerManager()
    {
        if (playerManager == null) playerManager = FindObjectOfType<PlayerManager>();
    }

    public void TeleportToScene1()
    {
        CheckPlayerManager();
        playerManager.TeleportPlayer(TeleportLocation1.position, TeleportLocation1.rotation, 2f);
        MenuWidgetController.Instance.ToggleMenuVisibility();
        
    }

    public void TeleportToScene2()
    {
        CheckPlayerManager();
        playerManager.TeleportPlayer(TeleportLocation2.position, TeleportLocation2.rotation, 1.15f);
        MenuWidgetController.Instance.ToggleMenuVisibility();
    }

    private void ToggleSceneVisibility()
    {
        switch (curLocation)
        {
            case CurrentLocation.Reception:
                // Turn off scene 1 stuff
                foreach(Renderer rend in scene1Renderers)
                {
                    rend.enabled = false;
                }
                foreach(Transform light in Scene1Lights)
                {
                    light.gameObject.SetActive(false);
                }

                // Turn on scene 2 stuff
                foreach (Renderer rend in scene2Renderers)
                {
                    rend.enabled = true;
                }
                foreach (Transform light in Scene2Lights)
                {
                    light.gameObject.SetActive(true);
                }
                break;
            case CurrentLocation.Ward:
                // Turn off scene 2 stuff
                foreach (Renderer rend in scene2Renderers)
                {
                    rend.enabled = false;
                }
                foreach (Transform light in Scene2Lights)
                {
                    light.gameObject.SetActive(false);
                }

                // Turn on scene 1 stuff
                foreach (Renderer rend in scene1Renderers)
                {
                    rend.enabled = true;
                }
                foreach (Transform light in Scene1Lights)
                {
                    light.gameObject.SetActive(true);
                }
                break;
        }
    }
}

﻿using extOSC;
using UnityEngine;

public class GameOSCManager : MonoBehaviour
{
    [HideInInspector]
    public OSCReceiver oscIn;

    private void Start()
    {
        if (!oscIn) oscIn = gameObject.AddComponent<OSCReceiver>();
        oscIn.LocalPort = 9009; // Igloo Web Snoop port
        oscIn.Connect();

        oscIn.Bind("/iglooWeb/key", IglooWebForwardKey);
        oscIn.Bind("/iglooWeb/hexkey", IglooWebRecieveHexKey);
    }



    #region Added Features

    public static string currentKeySent = "";
    private void IglooWebForwardKey(OSCMessage msg)
    {
        if (msg.Values.Count != 0)
        {
            currentKeySent = msg.Values[0].StringValue;
            MenuWidgetController.Instance.TypeIntoInputFieldUsingOSC(false);
        }
    }

    private void IglooWebRecieveHexKey(OSCMessage arg0)
    {
        if (arg0.Values.Count != 0)
        {
            switch (arg0.Values[0].StringValue)
            {
                case "0x08":
                    // BackSpace!!
                    MenuWidgetController.Instance.TypeIntoInputFieldUsingOSC(true);
                    break;

                case "0x0D":
                    // Carrige Return
                    MenuWidgetController.Instance.SaveGameFromCarridgeReturn();
                    break;
                default:
                    Debug.LogWarning("Unhandled String recieved : " + arg0.Values[0].StringValue);
                    break;
            }
        }
    }
    #endregion
}

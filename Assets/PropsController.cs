﻿using UnityEngine;
using Igloo;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This script handles every interactable object within the scene. 
/// It is also responsible for saving the objects that exist in the scene, their positions, and rotations.
/// It also holds the record of all scene objects that can be placed in the scene, which can be referenced 
/// by other scripts, I.E. the item creation menu. 
/// 
/// 
/// </summary>
public class PropsController : MonoBehaviour
{
    [Serializable]
    private struct SavedInteractableObject
    {
        public string prefabName;
        public Vector3 worldPosition;
        public Quaternion rotation; 
    }

    [System.Serializable]
    public struct InteractableObjects
    {
        [Tooltip("MUST BE UNIQUE")]
        public string prefabName;
        public GameObject prefab;
        public Sprite prefabIcon;

    }
    public InteractableObjects[] interactableObjects;

    private CharacterController playerCharController = null;

    private static PropsController instance;
    public static PropsController Instance
    {
        get
        {
            if (instance == null) instance = FindObjectOfType<PropsController>();
            return instance;
        }
    }

    public string currentSaveSlot = "";

    public void Start()
    {
        SaveSceneDefaults();
        playerCharController = FindObjectOfType<CharacterController>();
    }

    public void CreateNewProp(string propName)
    {
        for(int i = 0; i < interactableObjects.Length; i++)
        {
            if( interactableObjects[i].prefabName == propName)
            {
                var obj = Instantiate(interactableObjects[i].prefab, this.gameObject.transform, false);
                obj.transform.position = new Vector3(
                    PlayerPointer.Instance.attachObjectPoint.position.x,
                    Camera.main.transform.position.y - (playerCharController.height - 0.4f),
                    PlayerPointer.Instance.attachObjectPoint.position.z);

                obj.transform.localScale = Vector3.one;
                obj.name = interactableObjects[i].prefab.name;
                MenuWidgetController.Instance.ToggleMenuVisibility();

                // Stop looking through stack, found the object.
                break;
            }
        }
    }

    private GameObject GetPrefabFromName(string name)
    {
        for (int i = 0; i < interactableObjects.Length; i++)
        {
            if (interactableObjects[i].prefab.name == name)
            {
                return (interactableObjects[i].prefab);
            }
        }
        Debug.LogError($"Could not retrieve prefab with name: {name}");
        return null;
    }

    public void TryLoadGame(string saveGameName)
    {
        if (File.Exists(Application.persistentDataPath + $"/{saveGameName}.dat"))
        {
            Debug.Log("File exists, attempting to load save.");
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + $"/{saveGameName}.dat", FileMode.Open);
            SaveData data = (SaveData)bf.Deserialize(file);
            file.Close();
            SetupSceneFromLoad(data.savedInteractableObjects);
            Debug.Log("Game data loaded!");
        }
    }

    public List<string> GetAllSaveGames()
    {
        List<string> files = new List<string>();
        foreach (string file in Directory.GetFiles(Application.persistentDataPath))
        {
            // if the file is a .dat, and is not already part of the list. add it to the list.
            if (Path.GetExtension(file) == ".dat" && !files.Contains(Path.GetFileNameWithoutExtension(file)))
            {
                if (Path.GetFileNameWithoutExtension(file) == "Player" || Path.GetFileNameWithoutExtension(file) == "IglooDefaultSave") Debug.Log($"Skipping File : {file}");
                else files.Add(Path.GetFileNameWithoutExtension(file));
            }
                
        }
        return files;
    }

    /// <summary>
    /// Save the defaults to file, unless there is a save file already.
    /// </summary>
    void SaveSceneDefaults()
    {
        if (File.Exists(Application.persistentDataPath + "/IglooDefaultSave.dat")) return;
        else GetAllObjectsInSceneAndSave("IglooDefaultSave");
    }

    public void OnApplicationQuit()
    {
        GetAllObjectsInSceneAndSave(currentSaveSlot == "" ? "AutoBackup" : $"{currentSaveSlot}");
    }

    private void SetupSceneFromLoad(ObjectData[] savedInteractableObjects)
    {       
        // Destroy all the interactable Objects in the scene
        var objs = FindObjectsOfType<Interactable>();
        for (int i = 0; i < objs.Length; i++)
        {
            Destroy(objs[i].gameObject);
        }
        // Instantiate the prefabs, in their saved position and location
        for(int o = 0; o < savedInteractableObjects.Length; o++)
        {
            var obj = Instantiate(GetPrefabFromName(savedInteractableObjects[o].prefabName), this.gameObject.transform, false);
            obj.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            obj.transform.position = new Vector3(
                savedInteractableObjects[o].position[0], 
                savedInteractableObjects[o].position[1] + 0.3f, // small extra float to keep them from interacting with the floor
                savedInteractableObjects[o].position[2]
                );
            obj.transform.localScale = Vector3.one;
            obj.transform.rotation = Quaternion.Euler(new Vector3(
                savedInteractableObjects[o].rotation[0],
                savedInteractableObjects[o].rotation[1],
                savedInteractableObjects[o].rotation[2]
                ));
            obj.name = savedInteractableObjects[o].prefabName;
            StartCoroutine(RestoreRigidBody(obj.GetComponent<Rigidbody>()));
        }
    }

    private IEnumerator RestoreRigidBody(Rigidbody body)
    {
        yield return new WaitForSeconds(0.2f);
        body.constraints = RigidbodyConstraints.None;
        body.constraints = RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationX;
    }

    /// <summary>
    /// Can be called from a button on the UI, will delete the saved game, and restore all positions.
    /// Will restart the application #WARN
    /// </summary>
    public void ResetSceneToDefaults()
    {
        if (File.Exists(Application.persistentDataPath + "/IglooDefaultSave.dat"))
        {
            Debug.Log("Reloading save data positions");
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/IglooDefaultSave.dat", FileMode.Open);
            SaveData data = (SaveData)bf.Deserialize(file);
            file.Close();
            SetupSceneFromLoad(data.savedInteractableObjects);
            Debug.Log("Default data loaded!");
        }

        MenuWidgetController.Instance.ToggleMenuVisibility();
    }

    public void GetAllObjectsInSceneAndSave(string saveLoc)
    {
        var objs = GetComponentsInChildren<Interactable>();
        ObjectData[] newObjectData = new ObjectData[objs.Length - 1]; 

        for(int i = 0; i < newObjectData.Length; i++)
        {
            newObjectData[i].prefabName = objs[i].gameObject.name;
            newObjectData[i].position = new float[3];
            newObjectData[i].position[0] = objs[i].transform.position.x;
            newObjectData[i].position[1] = objs[i].transform.position.y;
            newObjectData[i].position[2] = objs[i].transform.position.z;
            newObjectData[i].rotation = new float[3];
            newObjectData[i].rotation[0] = objs[i].transform.rotation.eulerAngles.x;
            newObjectData[i].rotation[1] = objs[i].transform.rotation.eulerAngles.y;
            newObjectData[i].rotation[2] = objs[i].transform.rotation.eulerAngles.z;
        }

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + $"/{saveLoc}.dat");
        SaveData data = new SaveData
        {
            savedInteractableObjects = newObjectData
        };
        bf.Serialize(file, data);
        file.Close();
        Debug.Log("Game data saved!");
    }

    public bool DeleteSave(string saveName)
    {
        if (File.Exists(Application.persistentDataPath + $"/{saveName}.dat"))
        {
            File.Delete(Application.persistentDataPath + $"/{saveName}.dat");
            return true;
        }
        else return false;
    }

    [Serializable]
    class SaveData
    {
        public ObjectData[] savedInteractableObjects;
    }

    [Serializable]
    public struct ObjectData
    {
        public string prefabName;
        public float[] position;
        public float[] rotation;
    }
}


﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.Rendering;
using System.Collections.Generic;

public class MenuWidgetController : MonoBehaviour
{
    [Header("Options Page")]

    [SerializeField] private Volume _sceneVolume = null;
    private VolumeProfile _sceneVolProfile = null;
    private Exposure _exposureComponent = null;
    [SerializeField] private GameObject[] _directionSignObjects = null;

    [Header("Items Page")]
    [SerializeField] private GameObject _itemSelectionButtonPrefab = null;
    [SerializeField] private Transform _selectionGrid = null;

    [Header("Settings"), SerializeField] private GameObject _MenuWidget = null;
    [SerializeField] private AudioClip _clickMenuItemAudioClip = null;
    [SerializeField] private Slider _volumeSlider = null;
    [SerializeField] private Text _volumeSliderValueFeedback = null;
    private float _volumeLevel = 1.0f;
    [SerializeField] private Toggle _mutedToggle = null;
    private bool _muted = false;
    [SerializeField] private Toggle _ambientNoisesEnabledToggle = null;
    private bool _ambientNoisesEnabled = true;
    [SerializeField] private AudioSource _ambientAudioSource = null;
    [SerializeField] private Toggle _showButtonWidgetToggle = null;
    private bool _buttonWidgetEnabled = true;
    [SerializeField] private Toggle _showDirectionSignsToggle = null;
    private bool _directionSignsEnabled = true;
    [SerializeField] private Slider _uIScaleSlider = null;
    [SerializeField] private Text _uiScaleSliderValueFeedback = null;
    private float _uiScale = 1.0f;
    [SerializeField] private Slider _exposureControllerSlider = null;
    [SerializeField] private Text _exposureValueFeedback = null;
    private float _exposureValue = 5.0f;
    private string _newSaveGameName = "";
    [SerializeField] private Button _saveGameButton = null;
    [SerializeField] private Dropdown _loadGameDropdown = null;
    [SerializeField] private InputField _saveGameNameInputField = null;
    [SerializeField] private Text _currentLoadedGameName = null;
    [SerializeField] private Button _deleteCurrentLoadedGameButton = null;

    public static bool MenuIsOpen = false;

    private static MenuWidgetController _instance;
    public static MenuWidgetController Instance
    {
        get
        {
            if (_instance == null) _instance = FindObjectOfType<MenuWidgetController>();
            return _instance;
        }
    }
    #region Monodevelop Callbacks

    private void Start()
    {
        if (!_MenuWidget) Debug.LogError("No menu Widget attached to MenuWidgetController");
        CreateSelectionButtons();
        CheckAndLoadPlayerPrefs();
    }

    private void OnApplicationQuit()
    {
        SavePlayerPrefs();
    }

    private void OnApplicationFocus(bool focus)
    {
        SetMute(focus);
    }

    public void ToggleMenuVisibility()
    {
        if(_MenuWidget)
        {
            _MenuWidget.SetActive(!_MenuWidget.activeInHierarchy);
            MenuIsOpen = _MenuWidget.activeInHierarchy;
            Igloo.UIManager.Instance.ToggleUIVisibility(MenuIsOpen);
            if (MenuIsOpen)
            {
                ScanForSavedGames();
                
                UIControlsWidget.Instance.UpdateCurrentSelectionStatus(UIControlsWidget.CurrentSelectionStatus.OnMenu);
            }
            else
            {
                SavePlayerPrefs();
                UIControlsWidget.Instance.UpdateCurrentSelectionStatus(UIControlsWidget.CurrentSelectionStatus.OnNothing);
            }
        }
    }

    private void CheckAndLoadPlayerPrefs()
    {
        // Check for settings, and if there is one key, there will be all of them. 
        if (PlayerPrefs.HasKey("volumeLevel"))
        {
            // Global Volume
            _volumeLevel = PlayerPrefs.GetFloat("volumeLevel");
            _volumeSlider.value = _volumeLevel;
            SetGlobalVolume(_volumeLevel);
        }
        else
        {
            // Global volume slider
            _volumeSlider.value = _volumeLevel;
        }

        if (PlayerPrefs.HasKey("muted"))
        {
            // Global Mute
            _muted = PlayerPrefs.GetInt("muted") == 1 ? true : false;
            _mutedToggle.isOn = _muted;
            SetMute(_muted);
        }
        else
        {
            _mutedToggle.isOn = _muted;
        }

        if (PlayerPrefs.HasKey("ambientNoises"))
        {
            // Ambient noise switch
            _ambientNoisesEnabled = PlayerPrefs.GetInt("ambientNoises") == 1 ? true : false;
            _ambientNoisesEnabledToggle.isOn = _ambientNoisesEnabled;
            SetAmbientAudioEnabled(_ambientNoisesEnabled);
        }
        else
        {
            _ambientNoisesEnabledToggle.isOn = _ambientNoisesEnabled;
        }

        if (PlayerPrefs.HasKey("exposure"))
        {
            // Global Exposure
            _exposureValue = PlayerPrefs.GetFloat("exposure");
            _exposureControllerSlider.value = _exposureValue;
            AdjustExposureValue(_exposureValue);
        }
        else
        {
            // Set sceneExposureSlider from default exposure value
            if (_sceneVolume)
            {
                _sceneVolProfile = _sceneVolume.sharedProfile;
                if (_sceneVolProfile.TryGet(out _exposureComponent))
                {
                    _exposureValue = _exposureComponent.fixedExposure.value;
                    _exposureControllerSlider.value = _exposureValue;
                    _exposureValueFeedback.text = _exposureValue.ToString("0.00");
                }
            }
            Debug.Log("exposure defaults loaded, as no playerpref existed");
        }

        if (PlayerPrefs.HasKey("directionSigns"))
        {
            // Direction signs above doors
            _directionSignsEnabled = PlayerPrefs.GetInt("directionSigns") == 1 ? true : false;
            SetDirectionSignsEnabled(_directionSignsEnabled);
            _showDirectionSignsToggle.isOn = _directionSignsEnabled;
        }
        else
        {
            _showDirectionSignsToggle.isOn = _directionSignsEnabled;
        }

        if (PlayerPrefs.HasKey("uiScale"))
        {
            // UI Scale
            _uiScale = PlayerPrefs.GetFloat("uiScale");
            _uIScaleSlider.value = _uiScale;
            SetUIScale(_uiScale);
        }
        else
        {
            _uIScaleSlider.value = _uiScale;
        }

        if (PlayerPrefs.HasKey("buttonWidget"))
        {
            // Show Button Widget
            _buttonWidgetEnabled = PlayerPrefs.GetInt("buttonWidget") == 1 ? true : false;
            _showButtonWidgetToggle.isOn = _buttonWidgetEnabled;
            SetButtonWidgetEnabled(_buttonWidgetEnabled);
        }
        else // Load defaults
        {
            _showButtonWidgetToggle.isOn = _buttonWidgetEnabled;
        }

    }

    private void SavePlayerPrefs()
    {
        PlayerPrefs.SetFloat("volumeLevel", _volumeLevel);
        PlayerPrefs.SetInt("muted", _muted == true ? 1 : 0);
        PlayerPrefs.SetInt("ambientNoises", _ambientNoisesEnabled == true ? 1 : 0);
        PlayerPrefs.SetInt("directionSigns", _directionSignsEnabled == true ? 1 : 0);
        PlayerPrefs.SetFloat("exposure", _exposureValue);
        PlayerPrefs.SetFloat("uiScale", _uiScale);
        PlayerPrefs.SetInt("buttonWidget", _buttonWidgetEnabled == true ? 1 : 0);
        PlayerPrefs.Save();
        Debug.Log("Player preferences saved");
    }


    #endregion

    #region Items Page

    private void CreateSelectionButtons()
    {
        foreach(PropsController.InteractableObjects Intr in PropsController.Instance.interactableObjects)
        {
            var obj = Instantiate(_itemSelectionButtonPrefab, _selectionGrid, false);
            obj.GetComponent<Button>().onClick.AddListener(delegate () { PropsController.Instance.CreateNewProp(Intr.prefabName); });
            obj.GetComponent<Button>().onClick.AddListener(delegate () { Igloo.PlayerPointer.Instance.PlayAudioClip(_clickMenuItemAudioClip); });
            obj.GetComponentsInChildren<Image>()[1].sprite = Intr.prefabIcon;
            obj.GetComponentInChildren<Text>().text = Intr.prefabName;
        }
    }

    #endregion

    #region Options Page
    /// <summary>
    /// Adjusts the exposure value of the global volume effecting the entire scene.
    /// Effectivly increasing brightness.
    /// </summary>
    /// <param name="newVal"></param>
    public void AdjustExposureValue(float newVal)
    {
        _exposureValueFeedback.text = newVal.ToString("0.00");
        _exposureValue = newVal;
        if (_exposureComponent) _exposureComponent.fixedExposure.value = _exposureValue;
        else
        // Set sceneExposureSlider from default exposure value
        if (_sceneVolume)
        {
            _sceneVolProfile = _sceneVolume.sharedProfile;
            if (_sceneVolProfile.TryGet(out _exposureComponent))
            {
                _exposureComponent.fixedExposure.value = _exposureValue;
            }
        }
    }

    /// <summary>
    /// Uses the static AudioListener pause system to enable / disable audio output completely.
    /// Only one AudioListener is allowed in the scene.
    /// </summary>
    /// <param name="isMuted"></param>
    public void SetMute(bool isMuted)
    {
        _muted = isMuted;
        AudioListener.pause = isMuted;
    }

    /// <summary>
    /// Presumes that the ambient audio source has loop turned on, has a clip loaded.
    /// </summary>
    /// <param name="isEnabled"></param>
    public void SetAmbientAudioEnabled(bool isEnabled)
    {
        _ambientNoisesEnabled = isEnabled;
        if (isEnabled)
        {
            _ambientAudioSource.Play();
        }
        else
        {
            _ambientAudioSource.Stop();
        }
    }

    /// <summary>
    /// Called when a value is selected on the dropdown. 
    /// </summary>
    /// <param name="saveFileName"></param>
    public void LoadSaveGameFromDropdown(int dropdownValue)
    {
        string saveFileName = _loadGameDropdown.options[dropdownValue].text;
        PropsController.Instance.TryLoadGame(saveFileName);
        PropsController.Instance.currentSaveSlot = saveFileName;
        // Set the input field for this loaded save, so it's easy to save over it.
        _saveGameNameInputField.text = saveFileName;
        // set the save button so it's selectable.
        _saveGameButton.interactable = true;
        MenuWidgetController.Instance.ToggleMenuVisibility();
        _currentLoadedGameName.text = saveFileName;
        _deleteCurrentLoadedGameButton.interactable = true;
    }

    /// <summary>
    /// Sets the _newSaveGameName variable based on input from the InputField
    /// if the inputfield is not blank, then the save game button will be interactive.
    /// </summary>
    /// <param name="newSaveName"></param>
    public void SetSaveGameName(string newSaveName)
    {
        _newSaveGameName = newSaveName;
        if (newSaveName != "")
            _saveGameButton.interactable = true; // Set the save button, so it's selectable.
        else _saveGameButton.interactable = false; // Nothing entered, so make sure you can't save an empty file.
    }

    /// <summary>
    /// Saves the game, which is only able to happen when _newSaveGameName is not ""
    /// </summary>
    public void SaveGameButtonEvent()
    {
        PropsController.Instance.GetAllObjectsInSceneAndSave(_newSaveGameName);
        PropsController.Instance.currentSaveSlot = _newSaveGameName;
        _currentLoadedGameName.text = _newSaveGameName;
        _deleteCurrentLoadedGameButton.interactable = true;
        ScanForSavedGames();
    }

    public void SaveGameFromCarridgeReturn()
    {
        if (_saveGameNameInputField.isFocused)
        {
            SaveGameButtonEvent();
        }
    }

    /// <summary>
    /// Takes bool input, and loops through direction signs array setting them active or not.
    /// </summary>
    /// <param name="isEnabled"></param>
    public void SetDirectionSignsEnabled(bool isEnabled)
    {
        _directionSignsEnabled = isEnabled;
        Debug.Log($"Direction Signs enabled set to : {isEnabled}");
        for(int i = 0; i < _directionSignObjects.Length; i++)
        {
            _directionSignObjects[i].SetActive(isEnabled);
        }
    }

    /// <summary>
    /// Uses the static widget script to set the widgets visibility to clear for buttons
    /// and sets the text fields to blank. thus rendering it 'disabled' 
    /// </summary>
    /// <param name="isEnabled"></param>
    public void SetButtonWidgetEnabled(bool isEnabled)
    {
        _buttonWidgetEnabled = isEnabled;
        UIControlsWidget.Instance.ToggleVisibility(isEnabled);
    }

    /// <summary>
    /// Uses static AudioListener volume system to adjust global audio output.
    /// </summary>
    /// <param name="newVol"></param>
    public void SetGlobalVolume(float newVol)
    {
        _volumeLevel = newVol;
        _volumeSliderValueFeedback.text = _volumeLevel.ToString("0.00");
        AudioListener.volume = _volumeLevel;
    }

    /// <summary>
    /// TODO investigate how to achieve this easily.
    /// </summary>
    /// <param name="newUiScale"></param>
    public void SetUIScale(float newUiScale)
    {
        _uiScale = newUiScale;
        _uiScaleSliderValueFeedback.text = newUiScale.ToString("0.00");
    }

    /// <summary>
    /// Snoop the save game folder, for saved game files, and add them to the dropdown list.
    /// Set the current loaded game as the top of that list, if it exists.
    /// </summary>
    private void ScanForSavedGames()
    {
        _loadGameDropdown.ClearOptions();
        _loadGameDropdown.AddOptions(PropsController.Instance.GetAllSaveGames());
        if(_newSaveGameName != "")
        {
            _loadGameDropdown.SetValueWithoutNotify(GetIndexByName(_loadGameDropdown, PropsController.Instance.currentSaveSlot));
        }
    }

    /// <summary>
    /// Deletes the file containing the current loaded save. 
    /// If there is no current save, due to it being a default scene, or a save was 
    /// recently deleted. Then this is not possible due to the Delete button being inactive.
    /// </summary>
    public void DeleteCurrentLoadedSave()
    {
        if (PropsController.Instance.DeleteSave(PropsController.Instance.currentSaveSlot))
        {
            PropsController.Instance.ResetSceneToDefaults(); 
            _currentLoadedGameName.text = "No Layout Loaded";
            _saveGameNameInputField.text = "";
            _saveGameButton.interactable = false;
            _deleteCurrentLoadedGameButton.interactable = false;
            ScanForSavedGames();
        }
        else
        {
            Debug.LogError("Could not delete save game. Reason unsure...");
        }
    }

    #endregion

    #region Tools

    /// <summary>
    /// Function that looks for a string name within the special list type of a dropdown. 
    /// </summary>
    /// <param name="dropDown"></param>
    /// <param name="name"></param>
    /// <returns></returns>
    public int GetIndexByName(Dropdown dropDown, string name)
    {
        if (dropDown == null) { return -1; } // or exception
        if (string.IsNullOrEmpty(name) == true) { return -1; }
        List<Dropdown.OptionData> list = dropDown.options;
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].text.Equals(name)) { return i; }
        }
        return -1;
    }

    public void TypeIntoInputFieldUsingOSC(bool isBackspace)
    {
        if (_saveGameNameInputField.isFocused)
        {
            if (isBackspace)
            {
                if(_saveGameNameInputField.text != "")
                {
                    _saveGameNameInputField.text = _saveGameNameInputField.text.Remove(_saveGameNameInputField.text.Length - 1, 1);
                    SetSaveGameName(_saveGameNameInputField.text);
                }
            }
            else
            {
                _saveGameNameInputField.text += GameOSCManager.currentKeySent;
                SetSaveGameName(_saveGameNameInputField.text);
            }
        }
    }

    #endregion

}
